---
--- generate the Database : database_users_authentification
---
USE `skills_center_db`;

--- purge user if it already exists
DROP USER IF EXISTS 'adminApplication'@'localhost';

---
--- generate the profil for the User authentification and Registration
--- profil : admin (Grant All)
CREATE USER 'adminApplication'@'localhost' IDENTIFIED BY 'jXnhMVbB66dW2Vef@';
GRANT ALL PRIVILEGES ON * . * TO 'adminApplication'@'localhost';