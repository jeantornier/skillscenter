---
--- generate the Database : curricuulum_vitae_db
---
USE `skills_center_db`;

---
--- generate the views
--- 

-- view USERS_SECURITY_VIEW
CREATE OR REPLACE VIEW `users_security_view` AS 
SELECT users.username as 'User Name', 
		authorities.authority as 'Authority', 
		users.enabled as 'Enabled ?'
		
FROM skills_center_db.users as users, 
		skills_center_db.authorities as authorities,
		skills_center_db.users_authorities as users_authorities
		
WHERE users.id = users_authorities.users_id AND
	  authorities.id = users_authorities.authorities_id
	  
ORDER BY authorities.authority;

-- view USERS_REQUEESTS_SECURITY_VIEW
CREATE OR REPLACE VIEW `users_requests_security_view` AS 
SELECT users_requests.id as 'Request Id', 
		users_requests.username as 'For', 
		users_requests.request_date as 'Request Date', 
		users.username as 'Requester User Name', 
		authorities.authority as 'Authority',
		users_requests.enabled as 'Enabled ?'
		
FROM skills_center_db.users_requests as users_requests, 
		skills_center_db.authorities as authorities, 
        skills_center_db.users as users, 
        skills_center_db.authorities_users_requests as authorities_users_requests

WHERE users_requests.id = authorities_users_requests.users_requests_id AND 
		authorities.id = authorities_users_requests.authorities_id AND
        users_requests.users_id = users.id

ORDER BY authorities.authority;

-- view FULL_PROFILES_VIEW
CREATE OR REPLACE VIEW `full_profiles_view` AS
SELECT profiles.id as 'Profile Id', 
		users.username as 'User Name', 
		authorities.authority as 'Authority', 
		profiles.is_manager as 'Is Manager', 
		profiles.first_name as 'First Name', 
		profiles.last_name as 'Last Name', 
		profiles.email as 'Email Address', 
		profiles.title as 'Title', 
		profiles.full_experience_months as 'Experiences (months)', 
		ROUND(profiles.full_experience_months/12, 0) as 'Experiences (rounded to year)', 
		profiles.birth_date as 'Birth Date', 
		TIMESTAMPDIFF(YEAR, profiles.birth_date, CURDATE()) as 'Age', 
		profiles.hired_date as 'Hired Date', 
		TIMESTAMPDIFF(YEAR, profiles.hired_date, CURDATE()) as 'Hired (in year)', 
		profiles.locked as 'Is profile locked',
		departments.name as 'Department'
		
FROM skills_center_db.users as users, 
		skills_center_db.authorities as authorities, 
		skills_center_db.users_authorities as users_authorities, 
		skills_center_db.profiles as profiles, 
		skills_center_db.departments as departments
		
WHERE profiles.users_id = users.id AND 
	    users.id = users_authorities.users_id AND
		authorities.id = users_authorities.authorities_id AND 
		profiles.departments_id = departments.id
	
ORDER BY authorities.authority;
		
-- view PROFILES_SKILLS_VIEW
CREATE OR REPLACE VIEW `profiles_skills_view` AS
SELECT profiles.id as 'Profile Id', 
		users.username as 'User Name', 
		authorities.authority as 'Authority', 
		profiles.is_manager as 'Is Manager', 
		profiles.first_name as 'First Name', 
		profiles.last_name as 'Last Name', 
		profiles.email as 'Email Address', 
		profiles.title as 'Title', 
		profiles.full_experience_months as 'Experiences (months)', 
		ROUND(profiles.full_experience_months/12, 0) as 'Experiences (rounded to year)', 
		profiles.locked as 'Is profile locked',
		departments.name as 'Department',
		skills.name as 'Skill Name',
		profiles_skills.interest_rate as 'Interest (%)',
		profiles_skills.management_rate as 'Management (%)'
		
FROM skills_center_db.users as users, 
		skills_center_db.authorities as authorities, 
		skills_center_db.users_authorities as users_authorities, 
		skills_center_db.profiles as profiles, 
		skills_center_db.departments as departments,
		skills_center_db.profiles_skills as profiles_skills,
		skills_center_db.skills as skills
		
WHERE profiles.users_id = users.id AND 
		users.id = users_authorities.users_id AND
		authorities.id = users_authorities.authorities_id AND 
		profiles.departments_id = departments.id AND
		profiles.id = profiles_skills.profiles_id AND
		skills.id = profiles_skills.skills_id
		
ORDER BY users.username asc;

-- view PROFILES_CERTIFICATIONS_VIEW
CREATE OR REPLACE VIEW `profiles_certifications_view` AS
SELECT profiles.id as 'Profile Id', 
		users.username as 'User Name', 
		authorities.authority as 'Authority', 
		profiles.is_manager as 'Is Manager', 
		profiles.first_name as 'First Name', 
		profiles.last_name as 'Last Name', 
		profiles.email as 'Email Address', 
		profiles.title as 'Title', 
		profiles.full_experience_months as 'Experiences (months)', 
		ROUND(profiles.full_experience_months/12, 0) as 'Experiences (rounded to year)', 
		profiles.locked as 'Is profile locked',
		departments.name as 'Department',
		certifications.name as 'Certification Name',
		IF (certifications.validity_months = null, 'no limit', certifications.validity_months) as 'Certification Validity (in months)',
		profiles_certifications.certification_date as 'Certification Acquisition Date',
		IF (certifications.validity_months = null, 'valid', IF((certifications.validity_months-TIMESTAMPDIFF(MONTH, profiles_certifications.certification_date, CURDATE())) < 0, 'not valid', 'valid'))  as 'Certification Time Remaining (in months)',
		profiles_certifications.comments as 'Certification Comments'

FROM skills_center_db.users as users, 
		skills_center_db.authorities as authorities, 
		skills_center_db.users_authorities as users_authorities, 
		skills_center_db.profiles as profiles, 
		skills_center_db.departments as departments,
		skills_center_db.profiles_certifications as profiles_certifications,
		skills_center_db.certifications as certifications
		
WHERE profiles.users_id = users.id AND 
		users.id = users_authorities.users_id AND
		authorities.id = users_authorities.authorities_id AND 
		profiles.departments_id = departments.id AND
		profiles.id = profiles_certifications.profiles_id AND
		certifications.id = profiles_certifications.certifications_id
		
ORDER BY users.username asc;
		
--- view CERTIFICATIONS_VIEW
CREATE OR REPLACE VIEW `certifications_view` AS
SELECT certifications.id as 'Certification Id',
		certifications.name as 'Name',
		certifications.description as 'Description',
		certifiers.name as 'Is Assured By'

FROM skills_center_db.certifications as certifications, 
		skills_center_db.certifiers as certifiers, 
		skills_center_db.certifications_certifiers as certifications_certifiers

WHERE certifications.id = certifications_certifiers.certifications_id AND
		certifiers.id = certifications_certifiers.certifiers_id;

--- view _STATS_CERTIFICATIONS_VIEW
CREATE OR REPLACE VIEW `_stats_certifications_view` AS
SELECT certifications.id as 'Certification Id',
		certifications.name as 'Name',
		certifications.description as 'Description',
		count(*) as 'Number of certifiers'

FROM skills_center_db.certifications as certifications, 
		skills_center_db.certifiers as certifiers, 
		skills_center_db.certifications_certifiers as certifications_certifiers

WHERE certifications.id = certifications_certifiers.certifications_id AND
		certifiers.id = certifications_certifiers.certifiers_id
		
GROUP BY certifications.name;

--- view DEPARTMNENTS_VIEW
CREATE OR REPLACE VIEW `departments_view` AS
SELECT departments.id as 'Department Id',
		departments.name as 'Name',
		departments.description as 'Description',
		sections.name as 'Section bound'

FROM skills_center_db.departments as departments, 
		skills_center_db.sections as sections, 
		skills_center_db.departments_sections as departments_sections

WHERE departments.id = departments_sections.departments_id AND
		sections.id = departments_sections.sections_id;
		
--- view _STATS_DEPARTMNENTS_VIEW
CREATE OR REPLACE VIEW `_stats_departments_view` AS
SELECT departments.id as 'Department Id',
		departments.name as 'Name',
		departments.description as 'Description',
		count(*) as 'Number of sections bound'

FROM skills_center_db.departments as departments, 
		skills_center_db.sections as sections, 
		skills_center_db.departments_sections as departments_sections

WHERE departments.id = departments_sections.departments_id AND
		sections.id = departments_sections.sections_id

GROUP BY departments.name;

--- view SECTIONS_VIEW
CREATE OR REPLACE VIEW `sections_view` AS
SELECT sections.id as 'Section Id',
		sections.name as 'Name',
		sections.description as 'Description',
		skills.name as 'Skill bound'

FROM skills_center_db.sections as sections, 
		skills_center_db.skills as skills, 
		skills_center_db.skills_sections as skills_sections

WHERE sections.id = skills_sections.sections_id AND
		skills.id = skills_sections.skills_id;

--- view _STATS_SECTIONS_VIEW
CREATE OR REPLACE VIEW `_stats_sections_view` AS
SELECT sections.id as 'Section Id',
		sections.name as 'Name',
		sections.description as 'Description',
		count(*) as 'Number of skills bound'

FROM skills_center_db.sections as sections, 
		skills_center_db.skills as skills, 
		skills_center_db.skills_sections as skills_sections

WHERE sections.id = skills_sections.sections_id AND
		skills.id = skills_sections.skills_id

GROUP BY sections.name;