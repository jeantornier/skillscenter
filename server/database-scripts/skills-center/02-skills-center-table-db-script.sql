---
--- generate the Database : curricuulum_vitae_db
---
USE `skills_center_db`;

---
--- generate the tables
--- 

-- table DEPARTMENTS
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`name` varchar(50) UNIQUE NOT NULL,
	`description` text, 

	PRIMARY KEY (`id`)
	
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `departments` (name, description) VALUES ('Industry', 'description for Industry');
INSERT INTO `departments` (name, description) VALUES ('Technical IT', 'description for TI');
INSERT INTO `departments` (name, description) VALUES ('Health Care', 'description for Health Care');

-- table SECTIONS
DROP TABLE IF EXISTS `sections`;
CREATE TABLE `sections` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`name` varchar(50) UNIQUE NOT NULL,
	`description` text,
	
	PRIMARY KEY (`id`)
	
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `sections` (name, description) VALUES ('Front End', 'description for Front End');
INSERT INTO `sections` (name, description) VALUES ('Back End', 'description for Back End');
INSERT INTO `sections` (name, description) VALUES ('Database', 'description for Database');
INSERT INTO `sections` (name, description) VALUES ('GUI', 'description for GUI');


-- table DEPARTMENTS_SECTIONS
DROP TABLE IF EXISTS `departments_sections`;
CREATE TABLE `departments_sections` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`departments_id` int(11) NOT NULL,
	`sections_id` int(11) NOT NULL,
	
	PRIMARY KEY (`id`),
	UNIQUE KEY `departments_sections_unicity` (`departments_id`,`sections_id`),
	
	CONSTRAINT `departments_sections_ibfk_1` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`),
	CONSTRAINT `departments_sections_ibfk_2` FOREIGN KEY (`sections_id`) REFERENCES `sections` (`id`)
	
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `departments_sections` (departments_id, sections_id) VALUES (1, 3);
INSERT INTO `departments_sections` (departments_id, sections_id) VALUES (1, 4);

INSERT INTO `departments_sections` (departments_id, sections_id) VALUES (2, 1);
INSERT INTO `departments_sections` (departments_id, sections_id) VALUES (2, 2);
INSERT INTO `departments_sections` (departments_id, sections_id) VALUES (2, 3);

















-- table SKILLS
DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`name` varchar(50) UNIQUE NOT NULL,
	`description` text,
		
	PRIMARY KEY (`id`)
	
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `skills` (name, description) VALUES ('JavaScript', 'description of JavaScript');
INSERT INTO `skills` (name, description) VALUES ('ReactJS', 'description of ReactJS');
INSERT INTO `skills` (name, description) VALUES ('VueJS', 'description of VueJS');

INSERT INTO `skills` (name, description) VALUES ('Spring', 'description of Spring');
INSERT INTO `skills` (name, description) VALUES ('SpringBoot', 'description of SpringBoot');
INSERT INTO `skills` (name, description) VALUES ('SpringSecurity', 'description of SpringSecurity');

--- table SKILLS_SECTIONS
DROP TABLE IF EXISTS `skills_sections`;
CREATE TABLE `skills_sections` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`skills_id` int(11) NOT NULL,
	`sections_id` int(11) NOT NULL,

	PRIMARY KEY (`id`),
	UNIQUE KEY `skills_sections_unicity` (`skills_id`,`sections_id`),
	
	CONSTRAINT `skills_sections_ibfk_1` FOREIGN KEY (`skills_id`) REFERENCES `skills` (`id`),
	CONSTRAINT `skills_sections_ibfk_2` FOREIGN KEY (`sections_id`) REFERENCES `sections` (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


--- dump the table
INSERT INTO `skills_sections` (skills_id, sections_id) VALUES (1, 1);
INSERT INTO `skills_sections` (skills_id, sections_id) VALUES (2, 1);
INSERT INTO `skills_sections` (skills_id, sections_id) VALUES (3, 1);

INSERT INTO `skills_sections` (skills_id, sections_id) VALUES (4, 2);
INSERT INTO `skills_sections` (skills_id, sections_id) VALUES (4, 4);

INSERT INTO `skills_sections` (skills_id, sections_id) VALUES (5, 2);
INSERT INTO `skills_sections` (skills_id, sections_id) VALUES (5, 4);

INSERT INTO `skills_sections` (skills_id, sections_id) VALUES (6, 2);
INSERT INTO `skills_sections` (skills_id, sections_id) VALUES (6, 4);












--- table CERTIFIERS
DROP TABLE IF EXISTS `certifiers`;
CREATE TABLE `certifiers` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`name` varchar(50) UNIQUE NOT NULL,
	`description` text,

	PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `certifiers` (name, description) VALUES ('IBM', 'description for IBM');
INSERT INTO `certifiers` (name, description) VALUES ('Windows', 'description for Windows');
INSERT INTO `certifiers` (name, description) VALUES ('Cisco', 'description for Cisco');
INSERT INTO `certifiers` (name, description) VALUES ('SGS France', 'description for SGS France');
INSERT INTO `certifiers` (name, description) VALUES ('AFNOR', 'description for AFNOR');

--- table CERTIFICATIONS
DROP TABLE IF EXISTS `certifications`;
CREATE TABLE `certifications` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`name` varchar(50) UNIQUE NOT NULL,
	`description` text,
	`validity_months` int(11),

	PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `certifications` (name, description, validity_months) VALUES ('ISO8900', 'description for ISO8900', null);
INSERT INTO `certifications` (name, description, validity_months) VALUES ('IBM-10', 'description for IBM-10', 12);
INSERT INTO `certifications` (name, description, validity_months) VALUES ('IBM-20', 'description for IBM-20', 20);
INSERT INTO `certifications` (name, description, validity_months) VALUES ('ISO 14000', 'description for ISO 14000', 12);
INSERT INTO `certifications` (name, description, validity_months) VALUES ('ISO 9001', 'description for ISO 9001', null);
INSERT INTO `certifications` (name, description, validity_months) VALUES ('ISO 22000', 'description for ISO 22000', 6);
INSERT INTO `certifications` (name, description, validity_months) VALUES ('ISO 50001', 'description for ISO 50001', null);

--- table CERTIFICATIONS_CERTIFIERS
DROP TABLE IF EXISTS `certifications_certifiers`;
CREATE TABLE `certifications_certifiers` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`certifications_id` int(11) NOT NULL,
	`certifiers_id` int(11) NOT NULL,

	PRIMARY KEY (`id`),
	UNIQUE KEY `certifications_certifiers_unicity` (`certifications_id`,`certifiers_id`),
	
	CONSTRAINT `certifications_certifiers_ibfk_1` FOREIGN KEY (`certifications_id`) REFERENCES `certifications` (`id`),
	CONSTRAINT `certifications_certifiers_ibfk_2` FOREIGN KEY (`certifiers_id`) REFERENCES `certifiers` (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


--- dump the table
INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (1, 1);
INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (1, 2);
INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (1, 3);

INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (2, 1);
INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (2, 2);

INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (3, 1);

INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (4, 4);

INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (5, 5);

INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (6, 4);

INSERT INTO `certifications_certifiers` (certifications_id, certifiers_id) VALUES (7, 5);

--- table CERTIFICATIONS_DEPARTMENTS
DROP TABLE IF EXISTS `certifications_departments`;
CREATE TABLE `certifications_departments` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`certifications_id` int(11) NOT NULL,
	`departments_id` int(11) NOT NULL,

	PRIMARY KEY (`id`),
	UNIQUE KEY `certifications_departments_unicity` (`certifications_id`,`departments_id`),
	
	CONSTRAINT `certifications_departments_ibfk_1` FOREIGN KEY (`certifications_id`) REFERENCES `certifications` (`id`),
	CONSTRAINT `certifications_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `certifications_departments` (certifications_id, departments_id) VALUES (1, 1);
INSERT INTO `certifications_departments` (certifications_id, departments_id) VALUES (2, 2);
INSERT INTO `certifications_departments` (certifications_id, departments_id) VALUES (3, 2);


























--- table PROFILES
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`first_name` varchar(50) NOT NULL,
	`last_name` varchar(50) NOT NULL,
	`email` varchar(100) NOT NULL,
	
	`title` varchar(100) NOT NULL,
	
	`birth_date` timestamp,
	`hired_date` timestamp,

	`full_experience_months` int(11) NOT NULL DEFAULT 0,
	`is_manager` tinyint(1) NOT NULL DEFAULT 0,
	
	`users_id` int(11) NOT NULL,
	`departments_id` int(11),
	
	`locked` tinyint(1) NOt NULL,

	PRIMARY KEY (`id`),
	UNIQUE KEY `profiles_unicity_1` (`first_name`,`last_name`),
	UNIQUE KEY `profiles_unicity_2` (`email`),
	
	CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`),
	CONSTRAINT `profiles_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `profiles` (first_name, last_name, email, title, birth_date, hired_date, full_experience_months, is_manager, users_id, departments_id, locked) VALUES 
('super', 'super', 'super.super@amaris.com', 'super', null , null, 0, 0, 1, null, 1);
INSERT INTO `profiles` (first_name, last_name, email, title, birth_date, hired_date, full_experience_months, is_manager, users_id, departments_id, locked) VALUES 
('admin', 'admin', 'admin.admin@amaris.com', 'admin', null, null, 0, 0, 2, null, 1);
INSERT INTO `profiles` (first_name, last_name, email, title, birth_date, hired_date, full_experience_months, is_manager, users_id, departments_id, locked) VALUES 
('visitor', 'visitor', 'visitor.visitor@amaris.com', 'visitor', null, null, 0, 0, 3, null, 1);
INSERT INTO `profiles` (first_name, last_name, email, title, birth_date, hired_date, full_experience_months, is_manager, users_id, departments_id, locked) VALUES 
('jean', 'tornier', 'jean.tornier@amaris.com', 'developer', timestamp('1992-03-24 08:30:00'), timestamp('2019-12-02 00:00:00'), 48, 0, 4, 2, 0);

--- table PROFILES_SKILLS
DROP TABLE IF EXISTS `profiles_skills`;
CREATE TABLE `profiles_skills` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`profiles_id` int(11) NOT NULL,
	`skills_id` int(11) NOT NULL,
	
	`interest_rate` tinyint(1) NOT NULL DEFAULT 0,
	`management_rate` tinyint(1) NOT NULL DEFAULT 0,
	`experience_months` int(11) NOT NULL DEFAULT 0,
	
	`comments` text,

	PRIMARY KEY (`id`),
	UNIQUE KEY `profiles_skills_unicity` (`profiles_id`,`skills_id`),
	
	CONSTRAINT `profiles_skills_ibfk_1` FOREIGN KEY (`profiles_id`) REFERENCES `profiles` (`id`),
	CONSTRAINT `profiles_skills_ibfk_2` FOREIGN KEY (`skills_id`) REFERENCES `skills` (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `profiles_skills` (profiles_id, skills_id, interest_rate, management_rate, experience_months) VALUES (4, 4, 80, 50, 2);
INSERT INTO `profiles_skills` (profiles_id, skills_id, interest_rate, management_rate, experience_months) VALUES (4, 5, 70, 50, 2);
INSERT INTO `profiles_skills` (profiles_id, skills_id, interest_rate, management_rate, experience_months) VALUES (4, 6, 70, 50, 2);

--- table PROFILES_CERTIFICATIONS
DROP TABLE IF EXISTS `profiles_certifications`;
CREATE TABLE `profiles_certifications` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`profiles_id` int(11) NOT NULL,
	`certifications_id` int(11) NOT NULL,
	
	`certification_date` timestamp NOT NULL,
	`comments` text,
	
	PRIMARY KEY (`id`),
	UNIQUE KEY `profiles_skills_unicity` (`profiles_id`,`certifications_id`),
	
	CONSTRAINT `profiles_certifications_ibfk_1` FOREIGN KEY (`profiles_id`) REFERENCES `profiles` (`id`),
	CONSTRAINT `profiles_certifications_ibfk_2` FOREIGN KEY (`certifications_id`) REFERENCES `certifications` (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table
INSERT INTO `profiles_certifications` (profiles_id, certifications_id, certification_date) VALUES (4, 1, timestamp('2020-01-01'));
INSERT INTO `profiles_certifications` (profiles_id, certifications_id, certification_date) VALUES (4, 2, timestamp('2018-01-01'));
INSERT INTO `profiles_certifications` (profiles_id, certifications_id, certification_date) VALUES (4, 3, timestamp('2019-12-20'));
