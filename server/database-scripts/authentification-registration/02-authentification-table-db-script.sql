---
--- use the Database : database_users_authentification
---
USE `skills_center_db`;

---
--- generate the tables for the User Registration
--- 

--- table `authorities`
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE `authorities` (  

	`id` int(11) NOT NULL AUTO_INCREMENT,
	`authority` varchar(50) UNIQUE NOT NULL,
    
	PRIMARY KEY (`id`)
	
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table 
INSERT INTO `authorities` (authority) VALUES ('ROLE_SUPERADMIN');
INSERT INTO `authorities` (authority) VALUES ('ROLE_ADMIN');
INSERT INTO `authorities` (authority) VALUES ('ROLE_MANAGER');
INSERT INTO `authorities` (authority) VALUES ('ROLE_CONSULTANT');
INSERT INTO `authorities` (authority) VALUES ('ROLE_VISITOR');

--- table `users`
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (    

	`id` int(11) NOT NULL AUTO_INCREMENT,

	`username` varchar(50) UNIQUE NOT NULL,
	`password` varchar(100) NOT NULL,
    
	`enabled` tinyint(1) NOT NULL,
	
	PRIMARY KEY (`id`)
	
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--- dump the table 
INSERT INTO `users` (username, password, enabled) VALUES ('super', '{bcrypt}$2a$10$fUTv0yIruYmdR.3oL6QG6e9oF5uYJKwHZq0zIvaLE1hhoIZJcx/Jy', 1);
INSERT INTO `users` (username, password, enabled) VALUES ('admin', '{bcrypt}$2a$10$VJHWZHmDXQEJWcx1laXgouwprfz317RSbXrlGv5X8AgB14hYzqCFa', 1);
INSERT INTO `users` (username, password, enabled) VALUES ('visitor', '{bcrypt}$2a$10$eyuQP/.ZVC2YuR.AD42oNOzG37flvIWs.H5gHFx3RjrTdrQbXTNhK', 1);
INSERT INTO `users` (username, password, enabled) VALUES ('j.tornier', '{bcrypt}$2a$10$UxVtgDAqzge66931dBxNnONS0jOqwjVDWNw4QEHze/LsSdTa0N3v6', 1);

--- table `users_authorities`
DROP TABLE IF EXISTS `users_authorities`;
CREATE TABLE `users_authorities` (

	`users_id` int(11) NOT NULL,
	`authorities_id` int(11) NOT NULL,
	
	UNIQUE KEY `authorities_idx_1` (`users_id`,`authorities_id`),
	CONSTRAINT `users_authorities_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`),
	CONSTRAINT `users_authorities_ibfk_2` FOREIGN KEY (`authorities_id`) REFERENCES `authorities` (`id`)
		
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- dump the table 
INSERT INTO `users_authorities` (users_id, authorities_id) VALUES (1, 1);
INSERT INTO `users_authorities` (users_id, authorities_id) VALUES (2, 2);
INSERT INTO `users_authorities` (users_id, authorities_id) VALUES (3, 5);
INSERT INTO `users_authorities` (users_id, authorities_id) VALUES (4, 2);

--- table `users_requests`
DROP TABLE IF EXISTS `users_requests`;
CREATE TABLE `users_requests` (

	`id` int(11) NOT NULL AUTO_INCREMENT,
	
	`username` varchar(50) UNIQUE NOT NULL,
	`password` varchar(51000) NOT NULL,
	
	`enabled` tinyint(1) NOT NULL,
	`request_date` timestamp NOT NULL,
	
	`users_id` int(11) NOT NULL,
	
	PRIMARY KEY (`id`),
	CONSTRAINT `users_requests_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `users_requests` (username, password, enabled, request_date, users_id) VALUES ('a.janot', '{bcrypt}$2a$10$eyuQP/.ZVC2YuR.AD42oNOzG37flvIWs.H5gHFx3RjrTdrQbXTNhK', 1, timestamp('2020-01-06'), 4);
INSERT INTO `users_requests` (username, password, enabled, request_date, users_id) VALUES ('s.oz', '{bcrypt}$2a$10$eyuQP/.ZVC2YuR.AD42oNOzG37flvIWs.H5gHFx3RjrTdrQbXTNhK', 0, timestamp('2020-01-01'), 4);

--- table `authorities_users_requests`
DROP TABLE IF EXISTS `authorities_users_requests`;
CREATE TABLE `authorities_users_requests` (

	`id` int(11) NOT NULL AUTO_INCREMENT,

	`authorities_id` int(11) NOT NULL,
	`users_requests_id` int(11) NOT NULL,
	
    PRIMARY KEY(`id`),
    
	UNIQUE KEY `authorities_users_requests_idx_1` (`authorities_id`,`users_requests_id`),
	CONSTRAINT `authorities_users_requests_ibfk_1` FOREIGN KEY (`authorities_id`) REFERENCES `authorities` (`id`),
	CONSTRAINT `authorities_users_requests_ibfk_2` FOREIGN KEY (`users_requests_id`) REFERENCES `users_requests` (`id`)
		
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- dump the table 
INSERT INTO `authorities_users_requests` (authorities_id, users_requests_id) VALUES (4, 1);
INSERT INTO `authorities_users_requests` (authorities_id, users_requests_id) VALUES (2, 1);

INSERT INTO `authorities_users_requests` (authorities_id, users_requests_id) VALUES (3, 2);


