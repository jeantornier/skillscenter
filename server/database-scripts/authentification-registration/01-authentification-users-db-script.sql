---
--- generate the Database : skills_center_db
---
CREATE DATABASE IF NOT EXISTS `skills_center_db`;
USE `skills_center_db`;

--- purge user if it already exists
DROP USER IF EXISTS 'adminSecurity'@'localhost';

---
--- generate the profil for the User authentification and Registration
--- profil : adminSecurity (Grant All)
CREATE USER 'adminSecurity'@'localhost' IDENTIFIED BY '7KWUhUUmNJ5X7&Y';
GRANT ALL PRIVILEGES ON * . * TO 'adminSecurity'@'localhost';