package com.amaris.skillsCenter.repository.security;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amaris.skillsCenter.model.security.Users;

public interface UsersRepository extends JpaRepository<Users, Integer> {

	@Query("Select u from Users u where u.userName = ?1")
	Users findByUserName(String username);
	
	@Query("Select u from Users u where u.enabled = 0")
	List<Users> findAllUsersDisabled();
	
	@Query("Select u from Users u where u.enabled <> 0")
	List<Users> findAllUsersEnabled();
}
