package com.amaris.skillsCenter.repository.application;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amaris.skillsCenter.model.application.Certifiers;

public interface CertifiersRepository extends JpaRepository<Certifiers, Integer> {

	@Query("Select c from Certifiers c where c.name like %?1%")
	List<Certifiers> searchByName(String nameToSearch);
}
