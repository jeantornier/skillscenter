package com.amaris.skillsCenter.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amaris.skillsCenter.model.application.Departments;

public interface DepartmentsRepository extends JpaRepository<Departments, Integer> {

}
