package com.amaris.skillsCenter.repository.security;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.model.security.UsersRequests;

public interface UsersRequestsRepository extends JpaRepository<UsersRequests, Integer> {

	@Query("Select u from UsersRequests u where u.userName = ?1")
	UsersRequests findUsersRequestsByUserName(String username);
	
	@Query("Select u from UsersRequests u where u.enabled <> 1")
	List<UsersRequests> findUsersRequestsAsDisabled();
	
	@Query("Select u from UsersRequests u where u.enabled = 1")
	List<UsersRequests> findUsersRequestsAsEnabled();
	
	@Query("Select u from UsersRequests u where u.userRequester = ?1")
	List<UsersRequests> findUsersRequestsRequestedBy(Users theUserRequester);
	
	@Query("Select u from UsersRequests u left join u.authorities a where a = ?1")
	List<UsersRequests> findUsersRequestsForAuthority(Authorities theAuthorityToFind);
}
