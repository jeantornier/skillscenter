package com.amaris.skillsCenter.repository.application;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amaris.skillsCenter.model.application.Certifications;

public interface CertificationsRepository extends JpaRepository<Certifications, Integer> {

	@Query("Select c from Certifications c where c.name like %?1%")
	List<Certifications> searchByName(String nameToSearch);
	
	@Query("Select c from Certifications c where c.validityMonths = null")
	List<Certifications> searchCertificationsNoValidityLimitation();
	
	@Query("Select c from Certifications c where c.validityMonths <> null")
	List<Certifications> searchCertificationsWithValidityLimitation();
}
