package com.amaris.skillsCenter.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amaris.skillsCenter.model.application.Skills;

public interface SkillsRepository extends JpaRepository<Skills, Integer> {

}
