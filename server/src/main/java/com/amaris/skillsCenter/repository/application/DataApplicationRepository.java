package com.amaris.skillsCenter.repository.application;

import com.amaris.skillsCenter.model.application.DataApplication;

public interface DataApplicationRepository {
	
	public DataApplication getAllApplicationData();
	
	public String getApplicationName();
	public String getApplicationComments();
	
	public String getApplicationVersionMajor();
	public String getApplicationVersionMinor();
	public String getApplicationVersionPatch();
	
	public String applicationInDevelopment();
}
