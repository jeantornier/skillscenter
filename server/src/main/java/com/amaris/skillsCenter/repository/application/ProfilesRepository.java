package com.amaris.skillsCenter.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amaris.skillsCenter.model.application.Profiles;

public interface ProfilesRepository extends JpaRepository<Profiles, Integer> {

}
