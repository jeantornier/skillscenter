package com.amaris.skillsCenter.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amaris.skillsCenter.model.application.Sections;

public interface SectionsRepository extends JpaRepository<Sections, Integer> {

}
