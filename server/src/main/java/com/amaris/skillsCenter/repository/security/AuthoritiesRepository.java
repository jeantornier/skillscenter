package com.amaris.skillsCenter.repository.security;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.amaris.skillsCenter.model.security.Authorities;

public interface AuthoritiesRepository extends JpaRepository<Authorities, Integer> {
	
	@Query("Select a from Authorities a where a.authority = ?1")
	Authorities findByAuthorityLabel(String authoritylabel);
	
}
