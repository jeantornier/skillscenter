package com.amaris.skillsCenter.repository.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amaris.skillsCenter.model.application.DataApplication;

@Component
public class DataApplicationRepositoryImplXML implements DataApplicationRepository {
	
	@Autowired
	private DataApplication theDataApplication;
	
	@Override
	public DataApplication getAllApplicationData() {
		
		return theDataApplication;
	}
	
	@Override
	public String getApplicationName() {
		
		return theDataApplication.getApplicationName();
	}

	@Override
	public String getApplicationComments() {
		
		return theDataApplication.getComments();
	}

	@Override
	public String getApplicationVersionMajor() {
		
		return theDataApplication.getVersionMajor();
	}

	@Override
	public String getApplicationVersionMinor() {
		
		return theDataApplication.getVersionMinor();
	}

	@Override
	public String getApplicationVersionPatch() {

		return theDataApplication.getVersionPatch();
	}

	@Override
	public String applicationInDevelopment() {
		
		return theDataApplication.getDevelopment();
	}

}
