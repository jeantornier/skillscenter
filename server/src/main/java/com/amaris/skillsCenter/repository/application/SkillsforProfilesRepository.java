package com.amaris.skillsCenter.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amaris.skillsCenter.model.application.SkillsforProfiles;

public interface SkillsforProfilesRepository extends JpaRepository<SkillsforProfiles, Integer> {

}
