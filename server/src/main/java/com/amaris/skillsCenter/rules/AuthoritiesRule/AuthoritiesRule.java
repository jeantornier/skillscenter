package com.amaris.skillsCenter.rules.AuthoritiesRule;

import com.amaris.skillsCenter.model.security.Authorities;

public interface AuthoritiesRule {
	    	
    public String refactorAuthority(String authorityToRefactor);
    
	public boolean isSatified(Authorities theAuthority);
}