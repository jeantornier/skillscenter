package com.amaris.skillsCenter.rules.UsersRequestsRule;

import com.amaris.skillsCenter.model.security.UsersRequests;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsWrongPasswordException;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsWrongUserNameException;

public interface UsersRequestsRule {
	    	
    public String refactorUserName(String userNameToRefactor);
    public String refactorPassword(String passwordToRefactor);
    
	public boolean isSatified(UsersRequests theUserRequest)  throws UsersRequestsWrongUserNameException, UsersRequestsWrongPasswordException;
}