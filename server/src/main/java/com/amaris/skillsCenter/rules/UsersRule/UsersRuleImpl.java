package com.amaris.skillsCenter.rules.UsersRule;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amaris.skillsCenter.SkillsCenterApplication;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersWrongPasswordException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersWrongUserNameException;

/**
* The UsersRuleImpl class is the a specification rule for the Users.
* 
* This rule defines what to do about the UserName such as (from the 'rule.properties' file) :
*   - if the UserName has to be trim.
*   - if the UserName has to be set into lowerCase.
*   
* This rule defines what to do about the Password such as (from the 'rule.properties' file) :
*   - if the Password has to be trim.
*   
* @author  Jean TORNIER
* @version 0.1.0
* @since   2020-01-17
*/
@Component
public class UsersRuleImpl implements UsersRule {
	
    private static final Logger logger = LogManager.getLogger(SkillsCenterApplication.class);

    @Value("${rules.shared.usersRule.trim}")
	private String autoTrimConfig;
    @Value("${rules.shared.usersRule.lowerCase}")
	private String lowerCaseConfig;
    
    private boolean autoTrim;
    private boolean lowerCase;
    
    @PostConstruct
    public void refactor() { 
    	
    	setAutoTrimConfig(getAutoTrimConfig().trim().toLowerCase());
    	setLowerCaseConfig(getLowerCaseConfig().trim().toLowerCase());
    	
    	/* set the autoTrim and the lowerCase booleans variables 
    	 * from the String properties prepared below (from 'rules.properties' file) */
    	if(getAutoTrimConfig().equals("true")) { 
    		setAutoTrim(true);
    	} else {
    		setAutoTrim(false);
    	}
    	
    	if(getLowerCaseConfig().equals("true")) { 
    		setLowerCase(true);
    	} else {
    		setLowerCase(false);
    	}
    }
    
    /* Getters / Setters */
	public String getAutoTrimConfig() {
		return autoTrimConfig;
	}

	public void setAutoTrimConfig(String autoTrimConfig) {
		this.autoTrimConfig = autoTrimConfig;
	}

	public String getLowerCaseConfig() {
		return lowerCaseConfig;
	}

	public void setLowerCaseConfig(String lowerCaseConfig) {
		this.lowerCaseConfig = lowerCaseConfig;
	}

	public boolean isAutoTrim() {
		return autoTrim;
	}

	public void setAutoTrim(boolean autoTrim) {
		this.autoTrim = autoTrim;
	}

	public boolean isLowerCase() {
		return lowerCase;
	}

	public void setLowerCase(boolean lowerCase) {
		this.lowerCase = lowerCase;
	}
	
	/* ** ** ** ** 
	 * Rule methods
	 * ** ** ** ** */
	@Override
    public String refactorUserName(String userNameToRefactor) { 
		
		if (userNameToRefactor == null) { 
			return userNameToRefactor;
		}
		
		if (isAutoTrim()) { 
			userNameToRefactor = userNameToRefactor.trim();
		}
		
		if(isLowerCase()) { 
			userNameToRefactor = userNameToRefactor.toLowerCase();
		}
		
		return userNameToRefactor;
	}

	@Override
	public String refactorPassword(String passwordToRefactor) {
		
		if (passwordToRefactor == null) { 
			return passwordToRefactor;
		}
		
		if (isAutoTrim()) { 
			passwordToRefactor = passwordToRefactor.trim();
		}
		
		return passwordToRefactor;
	}

	@Override
	public boolean isSatified(Users theUser) throws UsersWrongUserNameException, UsersWrongPasswordException {
		
		/* if the UserName is null or if the UserName, after potential Trim and toLowerCase processing, is empty => raise the exception 'UsersWrongUserNameException' */
		if (theUser.getUserName() == null || refactorUserName(theUser.getUserName()).length() == 0) { 
			throw new UsersWrongUserNameException("the User, with UserName '" + theUser.getUserName() + "', breaks the Users Rule - it is NULL or empty.");
		}
		
		/* if the Password is null or if the UserName, after potential Trim processing, is empty => raise the exception 'UsersWrongPasswordException' */
		if (theUser.getPassword() == null || refactorPassword(theUser.getPassword()).length() == 0) { 
			throw new UsersWrongPasswordException("the User, with Password '" + theUser.getPassword() + "', breaks the Users Rule - it is NULL or empty.");
		}
		
		return true;	
	}
}
