package com.amaris.skillsCenter.rules.superAdministratorRule;

public interface Specification<T> {
	
    public boolean isSatisfiedBy();
}