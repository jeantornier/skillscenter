package com.amaris.skillsCenter.rules.AuthoritiesRule;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amaris.skillsCenter.SkillsCenterApplication;
import com.amaris.skillsCenter.model.security.Authorities;

/**
* The AuthoritiesRuleImpl class is the a specification rule for the Authorities.
* 
* This rule defines what to do about the Authority label such as (from the 'rule.properties' file) :
*   - if the Authority label has to be trim.
*   - if the UserName has to be set into lowerCase.
*   
* This rule defines what to do about the Password such as (from the 'rule.properties' file) :
*   - if the Password has to be trim.
*   
* @author  Jean TORNIER
* @version 0.1.0
* @since   2020-01-17
*/
@Component
public class AuthoritiesRuleImpl implements AuthoritiesRule {
	
    private static final Logger logger = LogManager.getLogger(SkillsCenterApplication.class);

    @Value("${rules.authoritiesRule.header}")
	private String headerConfig;
    @Value("${rules.authoritiesRule.trim}")
	private String autoTrimConfig;
    @Value("${rules.authoritiesRule.UPPERCASE}")
	private String upperCaseConfig;
    
    private String header;
    private boolean autoTrim;
    private boolean upperCase;
    
    @PostConstruct
    public void refactor() { 
    	
    	setHeaderConfig(getHeaderConfig().trim().toLowerCase());
    	setAutoTrimConfig(getAutoTrimConfig().trim().toLowerCase());
    	setUpperCaseConfig(getUpperCaseConfig().trim().toLowerCase());
    	
    	/* set the autoTrim and the upperCase booleans variables 
    	 * from the String properties prepared below (from 'rules.properties' file) */    	
    	if(getAutoTrimConfig().equals("true")) { 
    		setAutoTrim(true);
    	} else {
    		setAutoTrim(false);
    	}
    	
    	if(getUpperCaseConfig().equals("true")) { 
    		setUpperCase(true);
    	} else {
    		setUpperCase(false);
    	}
    	
    	/* set the header label : automatically trim */
    	setHeader(getHeaderConfig().trim());
    }
    
    /* Getters / Setters */
    public String getHeaderConfig() {
		return headerConfig;
	}

	public void setHeaderConfig(String headerConfig) {
		this.headerConfig = headerConfig;
	}

	public String getAutoTrimConfig() {
		return autoTrimConfig;
	}

	public void setAutoTrimConfig(String autoTrimConfig) {
		this.autoTrimConfig = autoTrimConfig;
	}

	public String getUpperCaseConfig() {
		return upperCaseConfig;
	}

	public void setUpperCaseConfig(String upperCaseConfig) {
		this.upperCaseConfig = upperCaseConfig;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public boolean isAutoTrim() {
		return autoTrim;
	}

	public void setAutoTrim(boolean autoTrim) {
		this.autoTrim = autoTrim;
	}

	public boolean isUpperCase() {
		return upperCase;
	}

	public void setUpperCase(boolean upperCase) {
		this.upperCase = upperCase;
	}    
	
	/* ** ** ** ** 
	 * Rule methods
	 * ** ** ** ** */
	@Override
    public String refactorAuthority(String authorityToRefactor) { 
		
		if (authorityToRefactor == null) {
			return authorityToRefactor;
		}
		
		if (isAutoTrim()) { 
			authorityToRefactor = authorityToRefactor.trim();
		}
		
		/* Make a none case snesitive comparison */
		if(authorityToRefactor.toLowerCase().indexOf(getHeader().toLowerCase()) != 0) { 
			authorityToRefactor = getHeader() + authorityToRefactor;
		}
		
		if(isUpperCase()) { 
			authorityToRefactor = authorityToRefactor.toUpperCase();
		}
		
		return authorityToRefactor;
	}

	@Override
	public boolean isSatified(Authorities theAuthority) {

		// TO DO
		return true;	
	}
}
