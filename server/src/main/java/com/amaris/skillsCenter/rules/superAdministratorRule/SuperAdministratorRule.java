package com.amaris.skillsCenter.rules.superAdministratorRule;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amaris.skillsCenter.SkillsCenterApplication;
import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.service.security.AuthoritiesService;
import com.amaris.skillsCenter.service.security.UsersService;

/**
* The SuperAdministratorRule class is the a specification rule for the administration of the SuperAdministrators.
* 
* The authority SUPER_ADMIN is reserved for a limit of User account. 
* In case this limit is reached, no more use can be set as SUPER_ADMIN.
* 
* To define and check this specification, 3 parameters are mandatory : 
*  - the SUPER_ADMIN label                    - for instance, 'ROLE_SUPERADMIN'
*  - the SUPER_ADMIN maximum accounts number  - for instance, 2
*  - the SUPER_ADMIN default username account - for instance, 'admin'
*  
* Rule 1: 
* 	the total number of user account, with the SUPER_ADMIN authority, can't exceed the maximal number of user set.
* Rule 2:
*   In case the total number of user account is reached, with the default user account set as SUPER_ADMIN, then, the default user account might lose its SUPER_ADMIN authority.
*   The new default user account is set with second authority.
* Rule 3: 
*   In case there is no user account, with the SUPER_ADMIN authority, the default user account is restore with the SUPER_ADMIN authority and lose its second authority of replacement.  
*
* @author  Jean TORNIER
* @version 0.1.0
* @since   2020-01-15
*/
@Component
public class SuperAdministratorRule implements Specification<Users> {
	
    private static final Logger logger = LogManager.getLogger(SkillsCenterApplication.class);

	@Autowired
	private UsersService usersService; 
	
	@Autowired
	private AuthoritiesService authoritiesService;
	
	/* Fields - retrieved from the database - by AspectJ */
	private Integer totalNumberOfUsers;

	private Users theDefaultUser;
	private Authorities superAdminAuthority;
	private Authorities replacementAuthority;
	private List<Users> allUsersSuperAdmin;
	
	/* Fields */
	@Value("${rules.superAdministratorRule.superAdministratorRole}")
	private String superAdministratorAuthorityLabelConfig;
	@Value("${rules.superAdministratorRule.replacementRole}")
	private String replacementAuthorityLabelConfig;
	@Value("${rules.superAdministratorRule.defaultUsername}")
	private String defaultSuperAdministratorAccountConfig;
	@Value("${rules.superAdministratorRule.maxAccountNumbers}")
	private String maxAccountNumbersConfig;

	private String superAdministratorAuthorityLabel;
	private String replacementAuthorityLabel;
	private String defaultSuperAdministratorAccount;
	private Integer maxAccountNumbers;
		
	@PostConstruct
	public void refactor() throws RuntimeException {
		
		replacementAuthorityLabel = replacementAuthorityLabelConfig.trim().toUpperCase();
		superAdministratorAuthorityLabel = superAdministratorAuthorityLabelConfig.trim().toUpperCase();
		
		defaultSuperAdministratorAccount = defaultSuperAdministratorAccountConfig.trim();
		
		maxAccountNumbers = Integer.parseInt(maxAccountNumbersConfig);
		if (maxAccountNumbers <= 0) {
			maxAccountNumbers = 1;
		}
		
		totalNumberOfUsers = null;
		superAdminAuthority = null;
		replacementAuthority = null;
		allUsersSuperAdmin = null;
	}
	
	/* Getters / Setters */
	public Integer getTotalNumberOfUsers() {
		return totalNumberOfUsers;
	}

	public void setTotalNumberOfUsers(Integer totalNumberOfUsers) {
		this.totalNumberOfUsers = totalNumberOfUsers;
	}

	public Users getTheDefaultUser() {
		return theDefaultUser;
	}

	public void setTheDefaultUser(Users theDefaultUser) {
		this.theDefaultUser = theDefaultUser;
	}

	public Authorities getSuperAdminAuthority() {
		return superAdminAuthority;
	}

	public void setSuperAdminAuthority(Authorities superAdminAuthority) {
		this.superAdminAuthority = superAdminAuthority;
	}

	public Authorities getReplacementAuthority() {
		return replacementAuthority;
	}

	public void setReplacementAuthority(Authorities replacementAuthority) {
		this.replacementAuthority = replacementAuthority;
	}

	public List<Users> getAllUsersSuperAdmin() {
		return allUsersSuperAdmin;
	}

	public void setAllUsersSuperAdmin(List<Users> allUsersSuperAdmin) {
		this.allUsersSuperAdmin = allUsersSuperAdmin;
	}

	public String getDefaultSuperAdministratorAccount() {
		return defaultSuperAdministratorAccount;
	}

	public void setDefaultSuperAdministratorAccount(String defaultSuperAdministratorAccount) {
		this.defaultSuperAdministratorAccount = defaultSuperAdministratorAccount;
	}

	public Integer getMaxAccountNumbers() {
		return maxAccountNumbers;
	}

	public void setMaxAccountNumbers(Integer maxAccountNumbers) {
		this.maxAccountNumbers = maxAccountNumbers;
	}
	
	

	/* ** ** ** ** ** **
	 * AspectJ methods
	 * ** ** ** ** ** ** */
	/**
	* 'retrieveAllSuperAdministratorsUsers' is a method, from the SuperAdministratorRule, retrieving all the Users with the SUPER_ADMIN authority.
	* This method is used by the AspectJ layer.
	* 
	* @param no arguments.
	* @return void.
	*/
	public void retrieveAllSuperAdministratorsUsers() {
		allUsersSuperAdmin = usersService.findUsersSuperAdmin();
		totalNumberOfUsers = allUsersSuperAdmin.size();
	}
	
	/**
	* 'retrieveSuperAdministratorAuthorities' is a method, from the SuperAdministratorRule, retrieving the SUPER_ADMIN authority.
	* This method is used by the AspectJ layer.
	* 
	* @param no arguments.
	* @return void.
	*/
	public void retrieveSuperAdministratorAuthorities() { 
		superAdminAuthority = authoritiesService.findByAuthority(superAdministratorAuthorityLabel);
	}
	
	/**
	* 'retrieveReplacementAuthorities' is a method, from the SuperAdministratorRule, retrieving the replacement authority for the default user.
	* This method is used by the AspectJ layer.
	* 
	* @param no arguments.
	* @return void.
	*/
	public void retrieveReplacementAuthorities() { 
		replacementAuthority = authoritiesService.findByAuthority(replacementAuthorityLabel);
	}
	
	/**
	* 'retrieveSuperAdministratorDefaultUsers' is a method, from the SuperAdministratorRule, retrieving the User defined as the default user.
	* This method is used by the AspectJ layer.
	* 
	* @param no arguments.
	* @return void.
	*/
	public void retrieveSuperAdministratorDefaultUsers() { 
		theDefaultUser = usersService.findByUserName(defaultSuperAdministratorAccount);
	}

	/* ** ** ** ** ** 
	 * Custom methods 
	 * ** ** ** ** ** */
	
	/**
	* 'isSatisfiedBy' is a method, from the SuperAdministratorRule, checking if the total number of user account with SUPER_ADMIN authority doesn't reach the limit set.
	* 
	* @param no arguments.
	* @return True if the total number reached or exceed the limit / False if the total number is below the limit .
	*/
	@Override
	public boolean isSatisfiedBy() {
		
		if (totalNumberOfUsers < maxAccountNumbers) {
			return true;
		}
		
		return false;
	}
	
	/**
	* 'findDefaultUser' is a method, from the SuperAdministratorRule, checking if in the list of Users, there is the default user account.
	* 
	* @param no arguments.
	* @return True if the default user is inside the list / False if the default user isn't inside the list.
	*/
	public boolean findDefaultUser(List<Users> theUsers) { 
		 
		for (Users theUser: theUsers) { 
			if (theUser.getUserName().equals(defaultSuperAdministratorAccount)) { 
				return true;
			}
		}
		
		return false;
	}
	
	/**
	* 'isUsersSuperAdmin' is a method, from the SuperAdministratorRule, checking if the User passed as input has, in its authorities, the SUPER_ADMIN authority.
	* In case the list of the authorities is NULL (never used), the result will be False (similar than with an empty list).
	* 
	* @param Users theUser.
	* @return True if the User has the SUPER_ADMIN authority / False if the User hasn't the SUPER_ADMIN authority.
	*/
	public boolean isUsersSuperAdmin(Users theUser) {
		
		if (theUser.getAuthorities() == null) { 
			return false;
		}
		
		for (Authorities theAuthority: theUser.getAuthorities()) { 
			if (theAuthority == superAdminAuthority) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	* 'isAuthoritySuperAdmin' is a method, from the SuperAdministratorRule, checking if the Authority, passed as input, is the SUPER_ADMIN authority.
	* 
	* @param Authorities theAuthorityToCheck
	* @return True if the Authority is the SUPER_ADMIN authority / False if the Authority isn't the SUPER_ADMIN authority.
	*/
	public boolean isAuthoritySuperAdmin(Authorities theAuthorityToCheck) {
		
		retrieveSuperAdministratorAuthorities();
		
		if (theAuthorityToCheck == getSuperAdminAuthority()) { 
			return true;
		}
		
		return false;
	}
	
	/**
	* 'removeSuperAdminAuthority' is a method, from the SuperAdministratorRule, which remove the SUPER_ADMIN authority from the authorities of the User passed as input.
	* 
	* @param Users theUser.
	* @return Users (without SUPER_ADMIN authority).
	*/
	public Users removeSuperAdminAuthority(Users theUser) { 
		theUser.removeAuthority(superAdminAuthority);
		return theUser;
	}
	
	/**
	* 'removeReplacementAuthority' is a method, from the SuperAdministratorRule, which remove the replacement authority from the authorities of the default User.
	* 
	* @param no arguments.
	* @return Users (without replacement authority).
	*/
	public Users removeReplacementAuthority() { 
		theDefaultUser.removeAuthority(replacementAuthority);
		return theDefaultUser;
	}
	
	/**
	* 'addSuperAdminAuthority' is a method, from the SuperAdministratorRule, which add the SUPER_ADMIN authority from the authorities of the User passed as input.
	* 
	* @param Users theUser.
	* @return Users (with SUPER_ADMIN authority).
	*/
	public Users addSuperAdminAuthority(Users theUser) { 
		theUser.addAuthority(superAdminAuthority);
		return theUser;
	}
	
	/**
	* 'addReplacementAuthority' is a method, from the SuperAdministratorRule, which add the replacement authority from the authorities of the default User.
	* 
	* @param no arguments.
	* @return Users (without replacement authority).
	*/
	public Users addReplacementAuthority() { 
		theDefaultUser.addAuthority(replacementAuthority);
		return theDefaultUser;
	}

	/**
	* 'attemptReduceSuperAdmin' is a method, from the SuperAdministratorRule, which replace the SUPER_ADMIN authority of the default User by the replacement authority.
	* At the end, the default user is saved.
	* 
	* @param no arguments.
	* @return void.
	*/
	public void attemptReduceSuperAdmin() { 
		
		logger.warn("SuperAdministrationRule.java - attemptReduceSuperAdmin - remove the SUPER ADMIN authority from the default User and add the replacement authority instead.");
		theDefaultUser.removeAuthority(superAdminAuthority);
		addReplacementAuthority();
		
		logger.warn("SuperAdministrationRule.java - attemptReduceSuperAdmin - save the default User with the replacement authority.");
		usersService.updateUser(theDefaultUser , false);
	}
	
	/**
	* 'attemptRestoreSuperAdmin' is a method, from the SuperAdministratorRule, which restore the SUPER_ADMIN authority of the default User and remove the replacement authority.
	* At the end, the default user is saved.
	* 
	* @param no arguments.
	* @return void.
	*/
	public void attemptRestoreSuperAdmin() { 
		
		logger.warn("SuperAdministrationRule.java - attemptRestoreSuperAdmin - add the SUPER ADMIN authority to the default User and remove the replacement authority.");
		theDefaultUser.addAuthority(superAdminAuthority);
		removeReplacementAuthority();
		
		logger.warn("SuperAdministrationRule.java - attemptRestoreSuperAdmin - save the default User with the SUPER ADMIN authority restored.");
		usersService.updateUser(theDefaultUser, false);
	}
}
