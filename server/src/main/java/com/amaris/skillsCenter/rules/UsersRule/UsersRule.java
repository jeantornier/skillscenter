package com.amaris.skillsCenter.rules.UsersRule;

import com.amaris.skillsCenter.model.security.Users;

public interface UsersRule {
	    	
    public String refactorUserName(String userNameToRefactor);
    public String refactorPassword(String passwordToRefactor);
    
	public boolean isSatified(Users theUser);
}