package com.amaris.skillsCenter.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:dataApplication.properties")
public class DataApplicationConfig {

	// contains the bean for the dataApplication.properties
}
