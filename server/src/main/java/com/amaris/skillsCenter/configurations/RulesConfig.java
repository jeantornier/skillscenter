package com.amaris.skillsCenter.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:rules.properties")
public class RulesConfig {

	// contains the bean for the rules.properties
}
