package com.amaris.skillsCenter.service.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.SkillsCenterApplication;
import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.model.wrappers.AuthentificationData;
import com.amaris.skillsCenter.model.wrappers.UsersAuthorities;
import com.amaris.skillsCenter.repository.security.UsersRepository;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersAuthentificationFailureException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersAuthentificationProcessFailureException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersLockedException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersNotFoundException;
import com.amaris.skillsCenter.rules.UsersRule.UsersRule;
import com.amaris.skillsCenter.rules.superAdministratorRule.SuperAdministratorRule;

/**
* The 'UsersServiceImpl' class is the implementation of the service layer for the 'Users'.
* This layer uses different DAO layers such as 'UsersRepository', 'AuthoritiesRepository'.
*
* @author  Jean TORNIER
* @version 0.1.0
* @since   2020-01-14
*/
@Service
public class UsersServiceImpl implements UsersService {
	
    private static final Logger logger = LogManager.getLogger(SkillsCenterApplication.class);

	@Autowired
	private SuperAdministratorRule superAdministratorRule;
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private AuthoritiesService authoritiesService;
	
	@Autowired
	private UsersRule usersRule;
	
	/**
	* 'existsById' is a method, from the service layer, allowing to check if a User, from the table 'Users', exists.
	* The User to find will be checked with its primary key : its 'id'.
	* 
	* @param int searchId.
	* @return boolean - True if the User exists / False if the User doesn't exists.
	*/
	@Override
	public boolean existsById(int searchId) {
		
		return usersRepository.existsById(searchId);
	}
	
	/* ** ** ** ** ** 
	 * Find methods 
	 * ** ** ** ** ** */
	/**
	* 'findById' is a method, from the service layer, allowing to retrieve a User, from the table 'Users', by its primary key : the 'id'.
	* In case the Id refers to nothing, the exception 'UsersNotFoundException' is raised.
	* 
	* @param int theId.
	* @return Users.
	* @exception UsersNotFoundException.
	*/
	@Override
	public Users findById(int theId) throws UsersNotFoundException {
		
		Optional<Users> result = usersRepository.findById(theId);
		Users theUser = null;
		
		if (result.isPresent()) {
			theUser = result.get();
		} else {
			throw new UsersNotFoundException("the user, with the Id '" + theId+ "', can't be found in the database !");
		}
		
		return theUser;
	}
	
	/**
	* 'findByUserName' is a method, from the service layer, allowing to find if a User, from the table 'Users', by its 'username'.
	*  In case the username doesn't refer to an existing User, then the exception 'UsersNotFoundException' is raised.
	* 
	* @param String username.
	* @return boolean - True if the User exists / False if the User doesn't exists.
	* @exception UsersNotFoundException.
	*/
	@Override
	public Users findByUserName(String username) throws UsersNotFoundException {
		
		username = usersRule.refactorUserName(username);
		Users theUserFound = usersRepository.findByUserName(username);
		
		if(theUserFound == null) { 
			throw new UsersNotFoundException("the user, with the username '" + username + "', can't be found in the database !");
		}
		
		return theUserFound;
	}
	
	/**
	* 'findUserSuperAdmin' is a method, from the service layer, allowing to retrieve all the User, from the table 'Users', defined as SUPERADMIN of the system.
	* 
	* @param no arguments.
	* @return List<Users>.
	*/
	@Override
	public List<Users> findUsersSuperAdmin() throws UsersNotFoundException { 
		
		List<Users> theUsersFound = usersRepository.findAll();
		List<Users> theUsersSuperAdmin = new ArrayList<Users>();
		
		for (Users theUser: theUsersFound) {
		
			if (superAdministratorRule.isUsersSuperAdmin(theUser) == true) {
				theUsersSuperAdmin.add(theUser);
			}
		}
		
		return theUsersSuperAdmin;
	}
	
	/**
	* 'findAllUsersDisabled' is a method, from the service layer, allowing to get the list of the Users disabled.
	* A user disabled has a property 'enabled' set to a value different than 1.
	* 
	* @param no arguments.
	* @return List<Users>.
	*/
	@Override
	public List<Users> findAllUsersDisabled() {
		
		return usersRepository.findAllUsersDisabled();
	}
	
	/**
	* 'findAllUsersEnabled' is a method, from the service layer, allowing to get the list of the Users enabled.
	* A user enabled has a property 'enabled' set to 1.
	* 
	* @param no arguments.
	* @return List<Users>.
	*/
	@Override
	public List<Users> findAllUsersEnabled() {
		
		return usersRepository.findAllUsersEnabled();
	}

	/**
	* 'createUser' is a method, from the service layer, allowing to create a User in the table 'Users'.
	*  The password is always encrypted with BCRYPT method.
	* 
	* @param Users theUsers.
	* @return void.
	*/
	@Override
	public void createUser(Users theUsers) {	
		
		theUsers.setPassword(AuthentificationData.hashPassword(theUsers.getPassword()));
		usersRepository.save(theUsers);
	}
	
	/**
	* 'updateUser' is a method, from the service layer, allowing to update a User in the table 'Users'.
	*  The password is always encrypted with BCRYPT method if the old password is changed.
	*  However, if the flag 'checkPasswordVariation' is set to False, then the password won't be checked (and changed).
	* 
	* @param Users theUsers.
	* @return void.
	*/
	@Override
	public void updateUser(Users theUsers, boolean checkPasswordVariation) {
		
		/* Get the old user to compare its password with the new one in order to know if a bcrypt encryption has to be done */
		Users oldUser = findById(theUsers.getId());
		
		if (checkPasswordVariation == true && !AuthentificationData.comparePasswords(theUsers.getPassword(), oldUser.getPassword())) {
			
			theUsers.setPassword(AuthentificationData.hashPassword(theUsers.getPassword()));
			
		} else {
			
			theUsers.setPassword(oldUser.getPassword());
		}
		
		usersRepository.save(theUsers);
	}

	/**
	* 'delete' is a method, from the service layer, allowing to delete a User, from the table 'Users'.
	* 
	* @param Users theUsers.
	* @return Users.
	* @exception UsersNotFoundException.
	*/
	@Override
	public Users delete(Users theUsers) throws UsersNotFoundException {
		
		/* reuse the method 'findById' of the class to get the User to delete
		 * if the user isn't found, will raise an exception*/
		Users theUserToDelete = findById(theUsers.getId());
		usersRepository.delete(theUserToDelete);;
		
		return theUserToDelete;
	}
	
	/**
	* 'deleteById' is a method, from the service layer, allowing to delete a User, from the table 'Users', by its primary key : the 'id'.
	* 
	* @param int theId.
	* @return Users.
	* @exception UsersNotFoundException.
	*/
	@Override
	public Users deleteById(int theId) throws UsersNotFoundException {
		
		/* reuse the method 'findById' of the class to get the User to delete
		 * if the user isn't found, will raise an exception*/
		Users theUserToDelete = findById(theId);
		usersRepository.deleteById(theUserToDelete.getId());
		
		return theUserToDelete;
	}
	
	/* ** ** ** ** ** 
	 * Specific methods 
	 * ** ** ** ** ** */
	/**
	* 'authentification' is a method, from the service layer, allowing to check if the data passed for the authentification, match with the records in database.
	* 
	* Step 1 : check if the user id passed, inside the data wrapper object, exists (by Id and then by username).
	*          -> if the user doesn't exist, raise the exception 'UsersNotFoundException'.
	* Step 2 : check if the user found is disabled.
	*          -> if the user is disabled, raise the exception 'UsersLockedException'.
	* Step 3 : check if the user found has the same password than the password passed in the data wrapper object.
	*          -> if the two passwords don't match, then the exception 'UsersAuthentificationFailureException' is raised.
	*          -> in case the passwords comparison failed ( due to another exception raised), the exception 'UsersAuthentificationProcessFailureException' is raised..
	* 
	* In case the authentification is a success, the User authenticated is returned.
	* 
	* @param AuthentificationData data.
	* @return Users.
	* @exception UsersNotFoundException, UsersLockedException, UsersAuthentificationFailureException, UsersAuthentificationProcessFailureException.
	*/
	@Override
	public Users authentification(AuthentificationData data) throws UsersNotFoundException, UsersLockedException, UsersAuthentificationFailureException, UsersAuthentificationProcessFailureException {
		
		Users theUserForAuthentification = null;
		
		try {
			
			theUserForAuthentification = findByUserName(data.getUsername()); 
		
		} catch (Exception exc) {

			throw new UsersNotFoundException("the user, with the username '" + data.getUsername() + "', can't be found in the database - authentification failed !");
		}
		
		if (theUserForAuthentification.getEnabled() != 1) {
			
			throw new UsersLockedException("the user, with the username '" + data.getUsername() + "', is locked in the database - authentification failed !");
		}
			
		/* compare passwords */
		boolean passwordCheck = false;
		try {
			
			passwordCheck = data.comparePasswords(theUserForAuthentification.getPassword());
			
		} catch (Exception exc) {
				
			/* comparePasswords() failed during the process */
			throw new UsersAuthentificationProcessFailureException("the user, with the username '" + data.getUsername() + "', can't be autentificated : the passwords comparison failed - authentification failed !");
		}	
			
		if (passwordCheck == true) {
				
				/* if password match - authentification is a success */
				return theUserForAuthentification;
				
		} else {

			/* if passwords didn't match - authentification failed */
			throw new UsersAuthentificationFailureException("the user, with the username '" + data.getUsername() + "', has a different password than the password saved in the database !");
		}	
	}
	
	/**
	* 'addAuthorities' is a method, from the service layer, allowing to add multiple authorities to a user.
	* This method uses the UsersAuthorities wrapper objects : {userId} -> {[authorityId]}.
	* If the authority, to add, is already associated to the User, it will be ignored.
	* 
	* note: in case the User requests to have the user authority SUPERADMIN, no oter User shall have it otherwise, the affectation will be ignored.
	* 
	* @param UsersAuthorities usersAuthorities.
	* @return List<Authorities>.
	* @exception UsersNotFoundException.
	*/
	@Override
	public List<Authorities> addAuthorities(UsersAuthorities usersAuthorities) throws UsersNotFoundException { 
		
		/* retrieve the User from the database, to update it with the list of the Authorities Id passed */
		Users theUser = findById(usersAuthorities.getUserId());
		logger.info("");
		logger.info("UsersServiceImpl.java - addAuthorities - INIT the process ...");

		/* For each authority Id passed, retrieve it from the database and add it to the User previously retrieved - authorities not found will be ignored */
		for (Integer authorityId: usersAuthorities.getAuthoritiesIds()) { 
			
			Authorities authorityToAdd = null;
			try { 
				authorityToAdd = authoritiesService.findById(authorityId);
			} catch (Exception exc) { 
				authorityToAdd = null;
			}
			
			if (authorityToAdd == null) { 
				logger.warn("UsersServiceImpl.java - addAuthorities - failed to find the authority to add, with the id '" + authorityId + "' -> ADD IGNORED !");
			} else {
				theUser.addAuthority(authorityToAdd);
			}	
		}
		
		/* Check if the user has the SUPER ADMIN authority - if not, save it immediately */
		if (superAdministratorRule.isUsersSuperAdmin(theUser) == false) { 
			
			usersRepository.save(theUser);
			
			/* if the database didn't have any SUPER ADMIN account - restore the default account (due to an eraseAuthorities) */
			if(superAdministratorRule.isSatisfiedBy() == true) {
				superAdministratorRule.attemptRestoreSuperAdmin();
			}
			
			logger.info("UsersServiceImpl.java - addAuthorities - END the process ...");
			return theUser.getAuthorities();
		}
		
		/* remove the SUPER ADMIN authority (can break the SuperAdministratorRule) temporary */
		theUser = superAdministratorRule.removeSuperAdminAuthority(theUser);
		if (superAdministratorRule.isSatisfiedBy() == true) {
			
			/* give back the SUPER ADMIN authority and save the user */
			theUser = superAdministratorRule.addSuperAdminAuthority(theUser);
			usersRepository.save(theUser);
			
			logger.info("UsersServiceImpl.java - addAuthorities - END the process ...");
			return theUser.getAuthorities();
		}
		
		logger.warn("UsersServiceImpl.java - addAuthorities - too much Users accounts found with the SUPER ADMIN authority ...");

		/* remove the default SUPER ADMIN user to save a user account and check again the rule */
		superAdministratorRule.attemptReduceSuperAdmin();
		if (superAdministratorRule.isSatisfiedBy() == true) {
			
			/* if the rule is now satisfied -> save the User with the SUPER ADMIN authority */
			theUser = superAdministratorRule.addSuperAdminAuthority(theUser);
			usersRepository.save(theUser);
			
			logger.info("UsersServiceImpl.java - addAuthorities - SUPER ADMIN has been affected to the User account '" + theUser.getUserName() + "' in place of the default User account.");
			logger.info("UsersServiceImpl.java - addAuthorities - END the process ...");
			return theUser.getAuthorities();
		}
		
		/* if the rule is still not satisfied -> ignore the SUPER ADMIN authority */
		logger.warn("UsersServiceImpl.java - addAuthorities - SUPER ADMIN can't be affected to the User account '" + theUser.getUserName() + "' -> this ONLY authority is IGNORED !");
		usersRepository.save(theUser);
		
		if(findUsersSuperAdmin().size() == 0) {
			superAdministratorRule.attemptRestoreSuperAdmin();
		}
		
		logger.info("UsersServiceImpl.java - addAuthorities - END the process ...");
		return theUser.getAuthorities();
	}
	
	/**
	* 'removeAuthorities' is a method, from the service layer, allowing to remove multiple authorities to a user.
	* This method uses the UsersAuthorities wrapper objects : {userId} -> {[authorityId]}.
	* If the authority, to remove, isn't associated to the User, it will be ignored.
	* 
	* @param UsersAuthorities usersAuthorities.
	* @return List<Authorities>.
	* @exception UsersNotFoundException.
	*/
	@Override
	public List<Authorities> removeAuthorities(UsersAuthorities usersAuthorities) throws UsersNotFoundException { 
		
		Users theUser = findById(usersAuthorities.getUserId());
		
		logger.info("");
		logger.info("UsersServiceImpl.java - removeAuthorities - INIT the process ...");
						
		/* Check if the Authority Id ( from the list) exists and remove them if there are associated to the User to update */
		for (Integer authorityId: usersAuthorities.getAuthoritiesIds()) { 
			
			Authorities authorityToRemove = null;
			try { 
				authorityToRemove = authoritiesService.findById(authorityId);
			} catch (Exception exc) { 
				authorityToRemove = null;
			}
			
			if (authorityToRemove == null) { 
				logger.warn("UsersServiceImpl.java - removeAuthorities - failed to find the authority to remove, with the id '" + authorityId + "' -> REMOVE IGNORED !");
			} else { 
				theUser.removeAuthority(authorityToRemove);	
			}
		}
				
		usersRepository.save(theUser);
		
		if (superAdministratorRule.isSatisfiedBy() == true) {
						
			logger.warn("UsersServiceImpl.java - removeAuthorities - User '" + theUser.getUserName() + "' lost the SUPER ADMIN authority and a User account, in the database, can receive the SUPER ADMIN authority.");
			superAdministratorRule.attemptRestoreSuperAdmin();
		}
		
		logger.info("UsersServiceImpl.java - removeAuthorities - END the process ...");
		return theUser.getAuthorities();
	}
	
	/**
	* 'eraseAuthorities' is a method, from the service layer, allowing to erase all the existing authorities, for a user, by other authorities.
	* This method uses the UsersAuthorities wrapper objects : {userId} -> {[authorityId]}.
	* 
	* @param UsersAuthorities usersAuthorities.
	* @return List<Authorities>.
	* @exception UsersNotFoundException.
	*/
	@Override
	public List<Authorities> eraseAuthorities(UsersAuthorities usersAuthorities) throws UsersNotFoundException { 
		
		Users theUser = findById(usersAuthorities.getUserId());
		theUser.purgeAuthorities();
		
		logger.info("");
		logger.info("UsersServiceImpl.java - eraseAuthorities - PURGE of all the authorities of the User '" + theUser.getUserName() + "'.");
						
		return addAuthorities(usersAuthorities);
	}
	
	/**
	* 'disableTheUser' is a method, from the service layer, allowing to disable the User account passed as input.
	* The user passed as input is retrieved again from the database to be certain that it exists (might raise a UsersNotFoundException).
	* This method will save the User enabled without updating its password.
	* 
	* @param Users theUserToDisable.
	* @return Users (disabled).
	* @exception UsersNotFoundException.
	*/
	public Users disableTheUser(Users theUserToDisable) throws UsersNotFoundException { 
		
		Users theUser = findById(theUserToDisable.getId());
		
		if(theUser.getEnabled() != 0) {
			theUser.setEnabled(0);
			updateUser(theUser, false);
		}
		
		return theUser;
	}
	
	/**
	* 'enableTheUser' is a method, from the service layer, allowing to enable the User account passed as input.
	* The user passed as input is retrieved again from the database to be certain that it exists (might raise a UsersNotFoundException).
	* This method will save the User enabled without updating its password.
	* 
	* @param Users theUserToEnable.
	* @return Users (enabled).
	* @exception UsersNotFoundException.
	*/
	public Users enableTheUser(Users theUserToEnable) throws UsersNotFoundException { 
		
		Users theUser = findById(theUserToEnable.getId());
		
		if(theUser.getEnabled() != 1) {
			theUser.setEnabled(1);
			updateUser(theUser, false);
		}
		
		return theUser;
	}
	
	/* ** ** ** ** **
	 * Custom methods
	 * ** ** ** ** ** */
	@Override
	public Users findUser(Users theUser) throws UsersNotFoundException { 
		
		/* extract the criteria for the find process */
		Integer theUserId = theUser.getId();
		String theUserName = usersRule.refactorUserName(theUser.getUserName());
		
		Users theUserFound = null;
		
		/* first attempt to find the User : search by ID */
		if (theUserId != null) { 
			
			try { 
				theUserFound = findById(theUserId);
				return theUserFound;
			} catch (UsersNotFoundException exc) {
				theUserFound = null;
			}
		}
		
		/* Second attempt to find the User : search by UserName */
		try { 
			theUserFound = findByUserName(theUserName);
			return theUserFound;
		} catch (UsersNotFoundException exc) {
			throw new UsersNotFoundException("the user [" + theUserId + "] '" + theUserName + "' can't be found in the database (searched by ID and username) !");
		}
	}
	
	
	
	
	
	
}
