package com.amaris.skillsCenter.service.application;

import java.util.List;

import com.amaris.skillsCenter.model.application.Certifications;

public interface CertificationsService {

	/* Exists methods */
	public boolean existsById(int searchId);
	
	/* Find methods */
	public List<Certifications> findAll();
	public Certifications findById(int theId);
	
	/* Save and Delete methods */
	public void save(Certifications theCertifications);	
	public void deleteById(int theId);
	
	/* Search methods */
	public List<Certifications> searchByName(String nameToSearch);
	public List<Certifications> searchCertificationsNoValidityLimitation();
	public List<Certifications> searchCertificationsWithValidityLimitation();
	
	/* Count methods */
	public long countAllCertifications();
}
