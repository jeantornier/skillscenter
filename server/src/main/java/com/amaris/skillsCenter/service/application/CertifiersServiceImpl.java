package com.amaris.skillsCenter.service.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.model.application.Certifiers;
import com.amaris.skillsCenter.repository.application.CertifiersRepository;

@Service
public class CertifiersServiceImpl implements CertifiersService {

	@Autowired
	private CertifiersRepository certifiersRepository;
	
	@Override
	public boolean existsById(int theId) {
		
		return certifiersRepository.existsById(theId);
	}
	
	@Override
	public List<Certifiers> findAll() {
		
		return certifiersRepository.findAll();
	}

	@Override
	public Certifiers findById(int theId) {
		
		Optional<Certifiers> result = certifiersRepository.findById(theId);
		Certifiers theCertifiers = null;
		
		if (result.isPresent()) {
			
			theCertifiers = result.get();
		}
		else {
			
			// we didn't find the employee
			throw new RuntimeException("Did not find theCertifiers id - " + theId);
		}
		
		return theCertifiers;
	}

	@Override
	public void save(Certifiers theCertifiers) {
		
		certifiersRepository.save(theCertifiers);
	}

	@Override
	public void deleteById(int theId) {

		certifiersRepository.deleteById(theId);
	}

	@Override
	public List<Certifiers> searchByName(String nameToSearch) {
		
		return certifiersRepository.searchByName(nameToSearch);
	}

	@Override
	public long countAllCertifiers() {
		
		return certifiersRepository.count();
	}
	
	
}

