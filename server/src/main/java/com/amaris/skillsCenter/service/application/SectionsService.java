package com.amaris.skillsCenter.service.application;

import java.util.List;

import com.amaris.skillsCenter.model.application.Sections;

public interface SectionsService {

	/* Find methods */
	public List<Sections> findAll();
	public Sections findById(int theId);
	
	/* Save and Delete methods */
	public void save(Sections theSections);	
	public void deleteById(int theId);
}
