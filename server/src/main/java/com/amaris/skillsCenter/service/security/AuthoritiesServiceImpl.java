package com.amaris.skillsCenter.service.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.SkillsCenterApplication;
import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.model.wrappers.UsersAuthorities;
import com.amaris.skillsCenter.repository.security.AuthoritiesRepository;
import com.amaris.skillsCenter.restController.errorHandlers.Authorities.AuthoritiesIllegalOperationException;
import com.amaris.skillsCenter.restController.errorHandlers.Authorities.AuthoritiesNotFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersNotFoundException;
import com.amaris.skillsCenter.rules.AuthoritiesRule.AuthoritiesRule;
import com.amaris.skillsCenter.rules.superAdministratorRule.SuperAdministratorRule;

@Service
public class AuthoritiesServiceImpl implements AuthoritiesService {

	private static final Logger logger = LogManager.getLogger(SkillsCenterApplication.class);

	@Value("${rules.authoritiesRule.header}")
	private String header;
	
	@Autowired
	private SuperAdministratorRule superAdministratorRule;
	
	@Autowired
	private AuthoritiesRepository authoritiesRepository;
	
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private AuthoritiesRule authoritiesRule;
	
	@PostConstruct
	public void refactor() { 
		
		/* Refactor the HEADER String (all in UPPERCASE) */
		if (getHeader() == null) {
			setHeader("ROLE_");
		}
		setHeader(header.toUpperCase());
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	/**
	* 'existsById' is a method, from the service layer, allowing to check if an Authority, from the table 'Authorities', exists.
	* The Authority to find will be checked with its primary key : its 'id'.
	* 
	* @param int searchId.
	* @return boolean - True if the Authority exists / False if the Authority doesn't exists.
	*/
	@Override
	public boolean existsById(int searchId) {
		return authoritiesRepository.existsById(searchId);
	}

	/* ** ** ** ** ** 
	 * Find methods 
	 * ** ** ** ** ** */
	/**
	* 'findAll' is a method, from the service layer, allowing to retrieve all the Authorities, from the table 'Authorities'.
	* 
	* @param no arguments.
	* @return List<Authorities>.
	*/
	@Override
	public List<Authorities> findAll() throws AuthoritiesNotFoundException {	
		return authoritiesRepository.findAll();
	}

	/**
	* 'findById' is a method, from the service layer, allowing to retrieve an Authority, from the table 'Authorities', by its primary key : the 'id'.
	* In case the Id refers to nothing, the exception 'AuthoritiesNotFoundException' is raised.
	* 
	* @param int theId.
	* @return Authorities.
	* @exception AuthoritiesNotFoundException.
	*/
	@Override
	public Authorities findById(int theId) throws AuthoritiesNotFoundException {
		
		Optional<Authorities> result = authoritiesRepository.findById(theId);
		Authorities theAuthority = null;
		
		if (result.isPresent()) {
			theAuthority = result.get();
		}
		else {
			throw new AuthoritiesNotFoundException("the authority, with the Id '" + theId + "', can't be found in the database !");
		}
		
		return theAuthority;
	}
	
	/**
	* 'findById' is a method, from the service layer, allowing to retrieve an Authority, from the table 'Authorities', by its primary key : the 'id'.
	* In case the Id refers to nothing, the exception 'AuthoritiesNotFoundException' is raised.
	* 
	* @param String theAuthorityLabel.
	* @return Authorities.
	* @exception AuthoritiesNotFoundException.
	*/
	@Override
	public Authorities findByAuthority(String theAuthorityLabel) throws AuthoritiesNotFoundException { 
		
		theAuthorityLabel = authoritiesRule.refactorAuthority(theAuthorityLabel);
		Authorities theAuthorityFound = authoritiesRepository.findByAuthorityLabel(theAuthorityLabel);
	
		if (theAuthorityFound == null) { 
			throw new AuthoritiesNotFoundException("the authority, with the authority label '" + theAuthorityLabel + "', can't be found in the database !");
		}
	
		return theAuthorityFound;
	}
	
	/**
	* 'findSuperAdminAuthority' is a method, from the service layer, allowing to retrieve the unique Authority, from the table 'Authorities', with is SUPER ADMIN.
	* 
	* @param no arguments.
	* @return Authorities.
	* @exception AuthoritiesNotFoundException.
	*/
	@Override
	public Authorities findSuperAdminAuthority() throws AuthoritiesNotFoundException { 
		
		Authorities theAuthorityFound = null;
		
		superAdministratorRule.retrieveSuperAdministratorAuthorities();
		theAuthorityFound = superAdministratorRule.getSuperAdminAuthority();
	
		if (theAuthorityFound == null) { 
			throw new AuthoritiesNotFoundException("the authority SUPER ADMIN can't be found in the database !");
		}
	
		return theAuthorityFound;
	}

	/**
	* 'createAuthority' is a method, from the service layer, allowing to create an Authority in the table 'Authorities'.
	* 
	* @param Authorities theAuthority.
	* @return void.
	*/
	@Override
	public void createAuthority(Authorities theAuthority) throws UsersNotFoundException {
			
		/* cast in UPPERCASE the Authority name and add, if needed, the Header String at the beginning of the label */
		theAuthority.setAuthority(theAuthority.getAuthority().trim().toUpperCase());
		if (theAuthority.getAuthority().indexOf(getHeader()) != 0) {
			theAuthority.setAuthority(getHeader() + theAuthority.getAuthority());
		}
		
		List<Users> theUsersToAssociate = theAuthority.getUsers();
		List<Users> theUsers = new ArrayList<Users>();
					
		/* save the authority here with no user inside
		 * user(s) association will be done later with a specific method to respect the SuperAdministratorRule */
		theAuthority.setUsers(new ArrayList<Users>());
		authoritiesRepository.save(theAuthority);
		
		/* if no user to associate to the authority created -> save and stop */
		if (theUsersToAssociate == null || theUsersToAssociate.size() == 0) { 
			return;
		}
			
		/* attempt to find all the users to associate to the new authority in the database
		 * in case a user doesn't exists, it will be ignored (logger.warn) */
		for(Users theUserToAssoicate: theUsersToAssociate) { 
			
			try { 
				Users theUser = usersService.findById(theUserToAssoicate.getId());
				theUsers.add(theUser);
			} catch(UsersNotFoundException exc) {
				logger.warn("In AuthoritiesServiceImpl.java - createAuthority - the User to find, with the Id '" + theUserToAssoicate.getId() + "' can't be found in the database -> it will be IGNORED !");
			}
		}
		
		for(Users theUser: theUsers) { 
			
			UsersAuthorities userAuthority = new UsersAuthorities(theUser.getId(), new ArrayList<Integer>());
			userAuthority.getAuthoritiesIds().add(theAuthority.getId());
			
			usersService.addAuthorities(userAuthority);
			logger.warn("In AuthoritiesServiceImpl.java - createAuthority - the User [" + theUser.getId() + "] " + theUser.getUserName() + " has been associated to the Authority [" + theAuthority.getId() + "] " + theAuthority.getAuthority());
		}
		
		/* finalize the creation by adding the Users to the authority to return */
		for(Users theUser: theUsers) { 
			theAuthority.addUser(theUser);
		}
	}
	
	/**
	* 'updateAuthority' is a method, from the service layer, allowing to update an Authority in the table 'Authorities'.
	* 
	* @param Authorities theAuthority.
	* @return void.
	* @exception AuthoritiesIllegalOperationException.

	*/
	@Override
	public void updateAuthority(Authorities theAuthority) throws AuthoritiesIllegalOperationException {
		
		/* cast in UPPERCASE the Authority name and add, if needed, the Header String at the beginning of the label */
		theAuthority.setAuthority(theAuthority.getAuthority().trim().toUpperCase());
		if (theAuthority.getAuthority().indexOf(getHeader()) != 0) {
			theAuthority.setAuthority(getHeader() + theAuthority.getAuthority());
		}
		
		Authorities authoritySaved = findById(theAuthority.getId());
		
		if (superAdministratorRule.isAuthoritySuperAdmin(authoritySaved) == true && !authoritySaved.getAuthority().equals(theAuthority.getAuthority())) { 
			throw new AuthoritiesIllegalOperationException("Attempt to update the label of the SUPER ADMINISTRATOR Authority - strictly forbidden in order to keep the database consistent.");
		}
		
		List<Users> theUsersToAssociate = theAuthority.getUsers();
		List<Users> theUsers = new ArrayList<Users>();

		theAuthority.setUsers(authoritySaved.getUsers());
		authoritiesRepository.save(theAuthority);
		
		/* if no user to associate to the authority updated -> save and stop */
		if (theUsersToAssociate == null || theUsersToAssociate.size() == 0) { 
			return;
		}
		
		for (Users theUserToAssociate: theUsersToAssociate) { 
			
			try { 
				Users theUser = usersService.findById(theUserToAssociate.getId());
				if(!theAuthority.getUsers().contains(theUser)) { 
					theUsers.add(theUser);
				} else {
					logger.warn("In AuthoritiesServiceImpl.java - updateAuthority - the User, with the Id [" + theUserToAssociate.getId() + "] " + theUserToAssociate.getUserName() + " is already associated to the Authority '" + theAuthority.getAuthority() + "' -> it will be IGNORED !");
				}
			} catch(UsersNotFoundException exc) {
				logger.warn("In AuthoritiesServiceImpl.java - updateAuthority - the User to find, with the Id '" + theUserToAssociate.getId() + "' can't be found in the database -> it will be IGNORED !");
			}
		}
		
		for(Users theUser: theUsers) { 
			
			UsersAuthorities userAuthority = new UsersAuthorities(theUser.getId(), new ArrayList<Integer>());
			userAuthority.getAuthoritiesIds().add(theAuthority.getId());
			
			usersService.addAuthorities(userAuthority);
			logger.warn("In AuthoritiesServiceImpl.java - updateAuthority - the User [" + theUser.getId() + "] " + theUser.getUserName() + " has been associated to the Authority [" + theAuthority.getId() + "] " + theAuthority.getAuthority());
		}
		
		/* finalize the update by adding the Users to the authority to return */
		for(Users theUser: theUsers) { 
			theAuthority.addUser(theUser);
		}
	}

	/**
	* 'delete' is a method, from the service layer, allowing to delete an Authority, from the table 'Authorities'.
	* Delete on the SUPER ADMIN authority is forbidden.
	* 
	* @param Authorities theAuthority.
	* @return Authorities.
	* @exception AuthoritiesNotFoundException, AuthoritiesIllegalOperationException.
	*/
	@Override
	public Authorities delete(Authorities theAuthority) throws AuthoritiesNotFoundException, AuthoritiesIllegalOperationException {
	
		/* reuse the method 'findById' of the class to get the Authority to delete
		 * if the authority isn't found, will raise an exception */
		Authorities theAuthorityToDelete = findById(theAuthority.getId());
		
		if (superAdministratorRule.isAuthoritySuperAdmin(theAuthorityToDelete) == true) { 
			throw new AuthoritiesIllegalOperationException("Attempt to delete the SUPER ADMINISTRATOR Authority - authority too important to be deleted.");
		}
		
		authoritiesRepository.delete(theAuthorityToDelete);
		return theAuthorityToDelete;
	}
	
	/**
	* 'deleteById' is a method, from the service layer, allowing to delete an Authority, from the table 'Authorities', by its primary key : the 'id'.
	* Delete on the SUPER ADMIN authority is forbidden.
	* 
	* @param int theId.
	* @return Authorities.
	* @exception AuthoritiesNotFoundException, AuthoritiesIllegalOperationException.
	*/
	@Override
	public Authorities deleteById(int theId) throws AuthoritiesNotFoundException, AuthoritiesIllegalOperationException {
		
		/* reuse the method 'findById' of the class to get the Authority to delete
		 * if the authority isn't found, will raise an exception */
		Authorities theAuthorityToDelete = findById(theId);
		
		if (superAdministratorRule.isAuthoritySuperAdmin(theAuthorityToDelete) == true) { 
			throw new AuthoritiesIllegalOperationException("Attempt to delete the SUPER ADMINISTRATOR Authority - authority too important to be deleted.");
		}
		
		authoritiesRepository.delete(theAuthorityToDelete);
		return theAuthorityToDelete;
	}
	
	
	/* ** ** ** ** **
	 * Custom methods
	 * ** ** ** ** ** */
	@Override
	public Authorities findAuthority(Authorities theAuthority) throws AuthoritiesNotFoundException { 
		
		/* extract the criteria for the find process */
		Integer theAuthorityId = theAuthority.getId();
		String theAuthorityLabel = authoritiesRule.refactorAuthority(theAuthority.getAuthority());
		
		Authorities theAuthoritytFound = null;
		
		/* first attempt to find the Authority : search by ID */
		if (theAuthorityId != null) { 
			
			try { 
				theAuthoritytFound = findById(theAuthorityId);
				return theAuthoritytFound;
			} catch (AuthoritiesNotFoundException exc) {
				theAuthoritytFound = null;
			}
		}
		
		/* Second attempt to find the Authority : search by UserName */
		try { 
			theAuthoritytFound = findByAuthority(theAuthorityLabel);
			return theAuthoritytFound;
		} catch (AuthoritiesNotFoundException exc) {
			throw new AuthoritiesNotFoundException("the authority [" + theAuthorityId + "] '" + theAuthorityLabel + "' can't be found in the database (searched by ID and authority label) !");
		}
	}
}
