package com.amaris.skillsCenter.service.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.model.application.Skills;
import com.amaris.skillsCenter.repository.application.SkillsRepository;

@Service
public class SkillsServiceImpl implements SkillsService {

	@Autowired
	private SkillsRepository skillsRepository;
	
	@Override
	public List<Skills> findAll() {
		
		return skillsRepository.findAll();
	}

	@Override
	public Skills findById(int theId) {
		
		Optional<Skills> result = skillsRepository.findById(theId);
		Skills theSkills = null;
		
		if (result.isPresent()) {
			
			theSkills = result.get();
		}
		else {
			
			// we didn't find the employee
			throw new RuntimeException("Did not find theSkills id - " + theId);
		}
		
		return theSkills;
	}

	@Override
	public void save(Skills theSkills) {
		
		skillsRepository.save(theSkills);
	}

	@Override
	public void deleteById(int theId) {
		
		skillsRepository.deleteById(theId);
	}

}
