package com.amaris.skillsCenter.service.application;

import com.amaris.skillsCenter.model.application.DataApplication;

public interface DataApplicationService {
	
	public DataApplication getAllApplicationData();
	
	public String getApplicationName();
	public String getApplicationComments();
	
	public String getApplicationVersion();
	public String getApplicationVersionMajor();
	public String getApplicationVersionMinor();
	public String getApplicationVersionPatch();
	
	public boolean isApplicationInDevelopment();
}
