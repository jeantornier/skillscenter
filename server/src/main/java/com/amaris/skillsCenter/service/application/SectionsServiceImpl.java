package com.amaris.skillsCenter.service.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.model.application.Sections;
import com.amaris.skillsCenter.repository.application.SectionsRepository;

@Service
public class SectionsServiceImpl implements SectionsService {

	@Autowired
	private SectionsRepository sectionsRepository;
	
	@Override
	public List<Sections> findAll() {
		
		return sectionsRepository.findAll();
	}

	@Override
	public Sections findById(int theId) {
		
		Optional<Sections> result = sectionsRepository.findById(theId);
		Sections theSections = null;
		
		if (result.isPresent()) {
			
			theSections = result.get();
		}
		else {
			
			// we didn't find the employee
			throw new RuntimeException("Did not find theSections id - " + theId);
		}
		
		return theSections;
	}

	@Override
	public void save(Sections theSections) {
		
		sectionsRepository.save(theSections);
	}

	@Override
	public void deleteById(int theId) {
		
		sectionsRepository.deleteById(theId);
	}

}
