package com.amaris.skillsCenter.service.security;

import java.util.List;

import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.restController.errorHandlers.Authorities.AuthoritiesNotFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersNotFoundException;

public interface AuthoritiesService {
	
	/* Exists methods */
	public boolean existsById(int searchId);
	
	/* Find methods */
	public List<Authorities> findAll();
	public Authorities findById(int theId) throws AuthoritiesNotFoundException;
	public Authorities findByAuthority(String theAuthorityLabel) throws AuthoritiesNotFoundException;
	
	public Authorities findAuthority(Authorities theAuthority) throws AuthoritiesNotFoundException;
	
	public Authorities findSuperAdminAuthority() throws AuthoritiesNotFoundException;
	
	/* Create, Update and Delete methods */
	public void createAuthority(Authorities theAuthority) throws UsersNotFoundException;	
	public void updateAuthority(Authorities theAuthority);	
	
	public Authorities delete(Authorities theAuthority) throws AuthoritiesNotFoundException;
	public Authorities deleteById(int theId) throws AuthoritiesNotFoundException;
}
