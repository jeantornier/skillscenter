package com.amaris.skillsCenter.service.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.model.application.SkillsforProfiles;
import com.amaris.skillsCenter.repository.application.SkillsforProfilesRepository;

@Service
public class SkillsforProfilesServiceImpl implements SkillsforProfilesService {

	@Autowired
	private SkillsforProfilesRepository skillsforProfilesRepository;
	
	@Override
	public List<SkillsforProfiles> findAll() {
		
		return skillsforProfilesRepository.findAll();
	}

	@Override
	public SkillsforProfiles findById(int theId) {
		
		Optional<SkillsforProfiles> result = skillsforProfilesRepository.findById(theId);
		SkillsforProfiles theSkillsforProfiles = null;
		
		if (result.isPresent()) {
			
			theSkillsforProfiles = result.get();
		}
		else {
			
			// we didn't find the employee
			throw new RuntimeException("Did not find theSkillsforProfiles id - " + theId);
		}
		
		return theSkillsforProfiles;
	}

	@Override
	public void save(SkillsforProfiles theSkillsforProfiles) {
		
		skillsforProfilesRepository.save(theSkillsforProfiles);
	}

	@Override
	public void deleteById(int theId) {
		
		skillsforProfilesRepository.deleteById(theId);
	}

}
