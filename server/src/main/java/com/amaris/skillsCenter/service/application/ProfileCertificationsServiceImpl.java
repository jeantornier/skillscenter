package com.amaris.skillsCenter.service.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.model.application.ProfileCertifications;
import com.amaris.skillsCenter.repository.application.ProfileCertificationsRepository;

@Service
public class ProfileCertificationsServiceImpl implements ProfileCertificationsService {

	@Autowired
	private ProfileCertificationsRepository profileCertificationsRepository;
	
	@Override
	public List<ProfileCertifications> findAll() {
		
		return profileCertificationsRepository.findAll();
	}

	@Override
	public ProfileCertifications findById(int theId) {
		
		Optional<ProfileCertifications> result = profileCertificationsRepository.findById(theId);
		ProfileCertifications theProfileCertifications = null;
		
		if (result.isPresent()) {
			
			theProfileCertifications = result.get();
		}
		else {
			
			// we didn't find the employee
			throw new RuntimeException("Did not find ProfileCertifications id - " + theId);
		}
		
		return theProfileCertifications;
	}

	@Override
	public void save(ProfileCertifications theProfilesCertifications) {
		
		profileCertificationsRepository.save(theProfilesCertifications);
	}

	@Override
	public void deleteById(int theId) {
		
		profileCertificationsRepository.deleteById(theId);
	}

}
