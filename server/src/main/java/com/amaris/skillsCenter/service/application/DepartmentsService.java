package com.amaris.skillsCenter.service.application;

import java.util.List;

import com.amaris.skillsCenter.model.application.Departments;

public interface DepartmentsService {
	
	/* Find methods */
	public List<Departments> findAll();
	public Departments findById(int theId);
	
	/* Save and Delete methods */
	public void save(Departments theDepartments);	
	public void deleteById(int theId);
}
