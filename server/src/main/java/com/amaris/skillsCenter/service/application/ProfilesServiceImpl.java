package com.amaris.skillsCenter.service.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.model.application.Profiles;
import com.amaris.skillsCenter.repository.application.ProfilesRepository;

@Service
public class ProfilesServiceImpl implements ProfilesService {

	@Autowired
	private ProfilesRepository profilesRepository;
	
	@Override
	public List<Profiles> findAll() {

		return profilesRepository.findAll();
	}

	@Override
	public Profiles findById(int theId) {
		
		Optional<Profiles> result = profilesRepository.findById(theId);
		Profiles theProfiles = null;
		
		if (result.isPresent()) {
			
			theProfiles = result.get();
		}
		else {
			
			// we didn't find the employee
			throw new RuntimeException("Did not find theProfiles id - " + theId);
		}
		
		return theProfiles;
	}

	@Override
	public void save(Profiles theProfiles) {
		
		profilesRepository.save(theProfiles);
	}

	@Override
	public void deleteById(int theId) {
		
		profilesRepository.deleteById(theId);
	}

}
