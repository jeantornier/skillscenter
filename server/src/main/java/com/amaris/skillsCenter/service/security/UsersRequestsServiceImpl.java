package com.amaris.skillsCenter.service.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.SkillsCenterApplication;
import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.model.security.UsersRequests;
import com.amaris.skillsCenter.model.wrappers.AuthentificationData;
import com.amaris.skillsCenter.repository.security.UsersRequestsRepository;
import com.amaris.skillsCenter.restController.errorHandlers.Authorities.AuthoritiesNotFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersNotFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsNoAuthoritiesException;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsNoRequesterException;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsNotFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsWrongPasswordException;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsWrongUserNameException;
import com.amaris.skillsCenter.rules.UsersRequestsRule.UsersRequestsRule;

@Service
public class UsersRequestsServiceImpl implements UsersRequestsService {
	
    private static final Logger logger = LogManager.getLogger(SkillsCenterApplication.class);
    
	@Autowired
	private UsersRequestsRepository usersRequestsRepository;
	
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private AuthoritiesService authoritiesService;
	
	@Autowired
	private UsersRequestsRule usersRequestsRule;

	/**
	* 'existsById' is a method, from the service layer, allowing to check if a User Request exists.
	* The User Request to find will be checked with its primary key : its 'id'.
	* 
	* @param int searchId.
	* @return boolean - True if the User Request exists / False if the User Request doesn't exists.
	*/
	@Override
	public boolean existsById(int searchId) {
		return usersRequestsRepository.existsById(searchId);
	}
		
	/* ** ** ** ** ** 
	 * Find methods 
	 * ** ** ** ** ** */
	/**
	* 'findAll' is a method, from the service layer, allowing to get all the User Requests.
	* 
	* @param no arguments.
	* @return List<UsersRequests>.
	*/
	@Override
	public List<UsersRequests> findAll() {
		return usersRequestsRepository.findAll();
	}
	
	/**
	* 'findById' is a method, from the service layer, allowing to a single User Request by its ID.
	* In case the ID doesn't refer to any User Request in the database, the exception 'UsersRequestsNotFoundException' will be raised.
	* 
	* @param int theId.
	* @return UsersRequests.
	* @exception UsersNotFoundException.
	*/
	@Override
	public UsersRequests findById(int theId) throws UsersRequestsNotFoundException {

		Optional<UsersRequests> result = usersRequestsRepository.findById(theId);
		UsersRequests theUsersRequests = null;
		
		if (result.isPresent()) {
			theUsersRequests = result.get();
		}
		else {
			throw new UsersRequestsNotFoundException("the user request, with the ID '" + theId + "', can't be found in the database !");
		}
		
		return theUsersRequests;
	}

	/**
	* 'findByUserName' is a method, from the service layer, allowing to a single User Request by its Username.
	* In case the Username doesn't refer to any User Request in the database, the exception 'UsersRequestsNotFoundException' will be raised.
	* The Username to search will be refactor according to the 'UsersRequestsRule'.
	* 
	* @param String username.
	* @return UsersRequests.
	* @exception UsersNotFoundException.
	*/
	@Override
	public UsersRequests findByUserName(String username) throws UsersRequestsNotFoundException {

		username = usersRequestsRule.refactorUserName(username);
		UsersRequests theUserRequestFound = usersRequestsRepository.findUsersRequestsByUserName(username);
		
		if(theUserRequestFound == null) { 
			throw new UsersRequestsNotFoundException("the user request, with the username '" + username + "', can't be found in the database !");
		}
		
		return theUserRequestFound;
	}

	/**
	* 'findAllRequestsDisabled' is a method, from the service layer, allowing to get all the User Request set with the property 'enabled' disabled.
	* Similar to the method 'findAllRequestsEnabled'.
	* 
	* @param no arguments.
	* @return List<UsersRequests>.
	*/
	@Override
	public List<UsersRequests> findAllRequestsDisabled() {
		return usersRequestsRepository.findUsersRequestsAsDisabled();
	}

	/**
	* 'findAllRequestsEnabled' is a method, from the service layer, allowing to get all the User Request set with the property 'enabled' enabled.
	* Similar to the method 'findAllRequestsDisabled'.
	* 
	* @param no arguments.
	* @return List<UsersRequests>.
	*/
	@Override
	public List<UsersRequests> findAllRequestsEnabled() {
		return usersRequestsRepository.findUsersRequestsAsEnabled();
	}

	/**
	* 'findAllRequestsByRequester' is a method, from the service layer, allowing to get all the User Request requested by the User passed as input.

	* @param Users theUserRequester.
	* @return List<UsersRequests>.
	*/
	@Override
	public List<UsersRequests> findAllRequestsByRequester(Users theUserRequester) {
		return usersRequestsRepository.findUsersRequestsRequestedBy(theUserRequester);
	}

	@Override
	public List<UsersRequests> findAllRequestsByAuthority(Authorities theAuthority) {
		return usersRequestsRepository.findUsersRequestsForAuthority(theAuthority);
	}

	@Override
	public void createUserRequest(UsersRequests theUserRequest) throws UsersRequestsWrongUserNameException, UsersRequestsWrongUserNameException, UsersRequestsNoRequesterException {
	
		Users theUserToRefer = theUserRequest.getUserRequester();
		
		List<Authorities> theAuthorities = theUserRequest.getAuthorities();
		List<Authorities> theAuthoritiesFinal = new ArrayList<Authorities>();
		
		/* refactor the UserName and the Password, according to the usersRequestsRule */
		theUserRequest.setUserName(usersRequestsRule.refactorUserName(theUserRequest.getUserName()));
		theUserRequest.setPassword(usersRequestsRule.refactorPassword(theUserRequest.getPassword()));
		
		/* isSatified will check if the UserRequest to save respect the UsersRequestsRule and throws exceptions if it doesn't */
		try {
			usersRequestsRule.isSatified(theUserRequest);
		} catch (UsersRequestsWrongUserNameException exc) {
			throw new UsersRequestsWrongUserNameException("the User Request, with UserName '" + theUserRequest.getUserName() + "', breaks the UsersRequests Rule - it is NULL or empty - create impossible !");
		} catch (UsersRequestsWrongPasswordException exc) {
			throw new UsersRequestsWrongPasswordException("the User Request, with Password '" + theUserRequest.getPassword() + "', breaks the UsersRequests Rule - it is NULL or empty - create impossible !");
		}

		/* Check the User Requester of the User Request to save - if the User Request is found -> add him to the User Request to create */
		if (theUserToRefer == null) { 
			throw new UsersRequestsNoRequesterException("the User Request, for the User '" + theUserRequest.getUserName() + "', can't be created correctly - no User Requester provided !");
		} else { 
			
			try { 
				theUserToRefer = usersService.findUser(theUserToRefer);
				theUserRequest.setUserRequester(theUserToRefer);
			} catch (UsersNotFoundException exc) {
				throw new UsersRequestsNoRequesterException("the theUsersRequests for '" + theUserRequest.getUserName() + "' can't be created correctly - the requester User provided can't be found in the database !");
			}
		}
		
		if(theAuthorities == null || theAuthorities.size() == 0) {
			throw new UsersRequestsNoAuthoritiesException("the theUsersRequests for '" + theUserRequest.getUserName() + "' can't be created correctly - at least an authority has to be provided !");
		} else { 
			
			for(Authorities theAuthority: theAuthorities) { 
				
				try { 
					Authorities theAuthorityFinal = authoritiesService.findAuthority(theAuthority);
					theAuthoritiesFinal.add(theAuthorityFinal);
				} catch (AuthoritiesNotFoundException exc) {
					logger.warn("UsersRequestsServiceImpl.java >> createUserRequest >> the authority of id '" + theAuthority.getId() + "' can't be found in the database - it will be ignored !");
				}
			}
			
			if(theAuthoritiesFinal.size() == 0) {
				throw new UsersRequestsNoAuthoritiesException("the User Request, for '" + theUserRequest.getUserName() + "', can't be created correctly - no existing and valid authority passed !");
			} else {
				theUserRequest.setAuthorities(theAuthoritiesFinal);
			}
		}
		
		/* set the request date to now and hash the password before saving the request */
		theUserRequest.setRequestDate(new Date());
		theUserRequest.setPassword(AuthentificationData.hashPassword(theUserRequest.getPassword()));
		usersRequestsRepository.save(theUserRequest);
	}

	@Override
	public void updateUserRequest(UsersRequests theUserRequest, UsersRequests oldUserRequestFound) {

		Users theUserToRefer = theUserRequest.getUserRequester();

		List<Authorities> theAuthorities = theUserRequest.getAuthorities();
		List<Authorities> theAuthoritiesFinal = new ArrayList<Authorities>();
		
		theUserRequest.setUserName(usersRequestsRule.refactorUserName(theUserRequest.getUserName()));
		theUserRequest.setPassword(usersRequestsRule.refactorPassword(theUserRequest.getPassword()));
		
		/* Reset the new User Request ID with the old one -> no need to change it */
		theUserRequest.setId(oldUserRequestFound.getId());
		
		/* if the UserName is set to NULL -> don't change it */
		if (theUserRequest.getUserName() == null) { 
			theUserRequest.setUserName(oldUserRequestFound.getUserName());
		}
		
		/* if the password changed -> BCRYPT it / else, use the old pasword (already bcrypted) */
		if (theUserRequest.getPassword() == null) { 
			theUserRequest.setPassword(oldUserRequestFound.getPassword());
		}
		
		/* if the Enabled is set to NULL -> don't change it */
		if (theUserRequest.getEnabled() == null) { 
			theUserRequest.setEnabled(oldUserRequestFound.getEnabled());
		}
				
		/* isSatified will check if the UserRequest to save respect the UsersRequestsRule and throws exceptions if it doesn't */
		try {
			usersRequestsRule.isSatified(theUserRequest);
		} catch (UsersRequestsWrongUserNameException exc) {
			throw new UsersRequestsWrongUserNameException("the User Request, with UserName '" + theUserRequest.getUserName() + "', breaks the UsersRequests Rule - it is NULL or empty - update impossible !");
		} catch (UsersRequestsWrongPasswordException exc) {
			throw new UsersRequestsWrongPasswordException("the User Request, with Password '" + theUserRequest.getPassword() + "', breaks the UsersRequests Rule - it is NULL or empty - update impossible !");
		}
		
		/* Check the User Requester of the User Request to update 
		 * if the new UserRequest doesn't provide a new User Requester, use the old one*/
		if (theUserToRefer == null) { 
			theUserRequest.setUserRequester(oldUserRequestFound.getUserRequester());
		} else { 
			
			try { 
				theUserToRefer = usersService.findUser(theUserToRefer);
				theUserRequest.setUserRequester(theUserToRefer);
			} catch (UsersNotFoundException exc) {
				throw new UsersRequestsNoRequesterException("the User Request, for '" + theUserRequest.getUserName() + "', can't be updated correctly - the requester User provided can't be found in the database !");
			}
		}
		
		/* Check the Authorities of the User Request to update 
		 * if the new Authorities don't provide any new authorities, use the old authorities. */
		if(theAuthorities == null || theAuthorities.size() == 0) {
			theUserRequest.setAuthorities(oldUserRequestFound.getAuthorities());
		} else { 
			
			for(Authorities theAuthority: theAuthorities) { 
				
				try { 
					Authorities theAuthorityFinal = authoritiesService.findAuthority(theAuthority);
					theAuthoritiesFinal.add(theAuthorityFinal);
				} catch (AuthoritiesNotFoundException exc) {
					logger.warn("UsersRequestsServiceImpl.java >> updateUserRequest >> the authority of id '" + theAuthority.getId() + "' can't be found in the database - it will be ignored !");
				}
			}
			
			if(theAuthoritiesFinal.size() == 0) {
				throw new UsersRequestsNoAuthoritiesException("the User Request, for '" + theUserRequest.getUserName() + "', can't be updated correctly - no existing and valid authority passed !");
			} else {
				theUserRequest.setAuthorities(theAuthoritiesFinal);
			}
		}
			
		/* set the request date to now and hash the password before saving the request
		 * refuse to update the Request Date -> use the OLD ONE ALWAYS */
		theUserRequest.setRequestDate(oldUserRequestFound.getRequestDate());
		theUserRequest.setPassword(AuthentificationData.hashPassword(theUserRequest.getPassword()));
		usersRequestsRepository.save(theUserRequest);		
	}

	@Override
	public UsersRequests delete(UsersRequests theUsersRequests) throws UsersRequestsNotFoundException {

		/* reuse the method 'findUserRequest' of the class to get the User Request to delete
		 * if the user request isn't found, will raise an exception */
		UsersRequests theUserRequestToDelete = findUserRequest(theUsersRequests);
		usersRequestsRepository.deleteById(theUserRequestToDelete.getId());
		
		return theUserRequestToDelete;
	}

	@Override
	public UsersRequests deleteById(int theId) throws UsersRequestsNotFoundException {
		
		/* reuse the method 'findUserRequest' of the class to get the User Request to delete
		 * if the user request isn't found, will raise an exception */
		UsersRequests theUserRequestToDelete = findById(theId);
		usersRequestsRepository.deleteById(theUserRequestToDelete.getId());
		
		return theUserRequestToDelete;
	}

	@Override
	public void validateUserRequest(UsersRequests theUsersRequests) {
		// TODO Auto-generated method stub
		
	}
	
	/* ** ** ** ** **
	 * Custom methods
	 * ** ** ** ** ** */
	@Override
	public UsersRequests findUserRequest(UsersRequests theUserRequest) throws UsersRequestsNotFoundException { 
		
		/* extract the criteria for the find process */
		Integer theUserRequestId = theUserRequest.getId();
		String theUserRequestUserName = usersRequestsRule.refactorUserName(theUserRequest.getUserName());
		
		UsersRequests theUserRequestFound = null;
		
		/* first attempt to find the UserRequest : search by ID */
		if (theUserRequestId != null) { 
			
			try { 
				theUserRequestFound = findById(theUserRequestId);
				return theUserRequestFound;
			} catch (UsersRequestsNotFoundException exc) {
				theUserRequestFound = null;
			}
		}
		
		/* Second attempt to find the UserRequest : search by UserName */
		try { 
			theUserRequestFound = findByUserName(theUserRequestUserName);
			return theUserRequestFound;
		} catch (UsersRequestsNotFoundException exc) {
			throw new UsersRequestsNotFoundException("the user request [" + theUserRequestId + "] '" + theUserRequestUserName + "' can't be found in the database (searched by ID and username) !");
		}
	}
}















