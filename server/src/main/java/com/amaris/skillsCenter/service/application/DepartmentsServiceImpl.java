package com.amaris.skillsCenter.service.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.model.application.Departments;
import com.amaris.skillsCenter.repository.application.DepartmentsRepository;

@Service
public class DepartmentsServiceImpl implements DepartmentsService {

	@Autowired
	private DepartmentsRepository departmentsRepository;
	
	@Override
	public List<Departments> findAll() {

		return departmentsRepository.findAll();
	}

	@Override
	public Departments findById(int theId) {

		Optional<Departments> result = departmentsRepository.findById(theId);
		Departments theDepartments = null;
		
		if (result.isPresent()) {
			
			theDepartments = result.get();
		}
		else {
			
			// we didn't find the employee
			throw new RuntimeException("Did not find theDepartments id - " + theId);
		}
		
		return theDepartments;
	}

	@Override
	public void save(Departments theDepartments) {
		
		departmentsRepository.save(theDepartments);
	}

	@Override
	public void deleteById(int theId) {
		
		departmentsRepository.deleteById(theId);
	}
}
