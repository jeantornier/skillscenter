package com.amaris.skillsCenter.service.application;

import java.util.List;

import com.amaris.skillsCenter.model.application.ProfileCertifications;

public interface ProfileCertificationsService {
	
	/* Find methods */
	public List<ProfileCertifications> findAll();
	public ProfileCertifications findById(int theId);
	
	/* Save and Delete methods */
	public void save(ProfileCertifications theProfilesCertifications);	
	public void deleteById(int theId);
}
