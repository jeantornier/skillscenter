package com.amaris.skillsCenter.service.security;

import java.util.List;

import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.model.security.UsersRequests;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsNotFoundException;

public interface UsersRequestsService {
	
	/* Exists methods */
	public boolean existsById(int searchId);
	
	/* Find methods */
	public List<UsersRequests> findAll(); 
	public UsersRequests findById(int theId) throws UsersRequestsNotFoundException;
	public UsersRequests findByUserName(String username) throws UsersRequestsNotFoundException;
		
	public List<UsersRequests> findAllRequestsDisabled();
	public List<UsersRequests> findAllRequestsEnabled();
	
	public List<UsersRequests> findAllRequestsByRequester(Users theUser);
	public List<UsersRequests> findAllRequestsByAuthority(Authorities theAuthority);

	/* Save and Delete methods */
	public void createUserRequest(UsersRequests theUserRequest);	
	public void updateUserRequest(UsersRequests theUserRequest, UsersRequests oldUserRequestFound);	
	
	public UsersRequests delete(UsersRequests theUsersRequests);
	public UsersRequests deleteById(int theId);
	
	/* Accept a User request saved and submitted */
	public void validateUserRequest(UsersRequests theUsersRequests) throws UsersRequestsNotFoundException ;	

	/* Custom methods */
	public UsersRequests findUserRequest(UsersRequests theUserRequest);


}
