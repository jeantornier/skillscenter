package com.amaris.skillsCenter.service.security;

import java.util.List;

import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.model.wrappers.AuthentificationData;
import com.amaris.skillsCenter.model.wrappers.UsersAuthorities;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersAuthentificationFailureException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersAuthentificationProcessFailureException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersLockedException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersNotFoundException;

public interface UsersService {

	/* Exists methods */
	public boolean existsById(int searchId);
	
	/* Find methods */
	public Users findById(int theId) throws UsersNotFoundException;
	public Users findByUserName(String username) throws UsersNotFoundException;
	public List<Users> findUsersSuperAdmin() throws UsersNotFoundException;
	
	public Users findUser(Users theUser) throws UsersNotFoundException;
	
	public List<Users> findAllUsersDisabled();
	public List<Users> findAllUsersEnabled();

	/* Save and Delete methods */
	public void createUser(Users theUsers);	
	public void updateUser(Users theUsers, boolean checkPasswordVariation);	
	
	public Users delete(Users theUsers) throws UsersNotFoundException;
	public Users deleteById(int theId) throws UsersNotFoundException;
	
	/* For the authentification */
	public Users authentification(AuthentificationData data) throws UsersNotFoundException, UsersLockedException, UsersAuthentificationFailureException, UsersAuthentificationProcessFailureException;
	
	/* ADD and REMOVE authorities */
	public List<Authorities> addAuthorities(UsersAuthorities usersAuthorities) throws UsersNotFoundException;
	public List<Authorities> removeAuthorities(UsersAuthorities usersAuthorities) throws UsersNotFoundException;

	public List<Authorities> eraseAuthorities(UsersAuthorities usersAuthorities) throws UsersNotFoundException;
	
	/* Disable / Enable the User */
	public Users disableTheUser(Users theUserToDisable) throws UsersNotFoundException;
	public Users enableTheUser(Users theUserToEnable) throws UsersNotFoundException;
	
}
