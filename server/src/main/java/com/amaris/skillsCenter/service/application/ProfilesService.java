package com.amaris.skillsCenter.service.application;

import java.util.List;

import com.amaris.skillsCenter.model.application.Profiles;

public interface ProfilesService {

	/* Find methods */
	public List<Profiles> findAll();
	public Profiles findById(int theId);
	
	/* Save and Delete methods */
	public void save(Profiles theProfiles);	
	public void deleteById(int theId);
}
