package com.amaris.skillsCenter.service.application;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.model.application.Certifications;
import com.amaris.skillsCenter.repository.application.CertificationsRepository;

@Service
public class CertificationsServiceImpl implements CertificationsService {

	@Autowired
	private CertificationsRepository certificationsRepository;
	
	@Override
	public boolean existsById(int searchId) {

		return certificationsRepository.existsById(searchId);
	}
	
	@Override
	public List<Certifications> findAll() {
		
		return certificationsRepository.findAll();
	}

	@Override
	public Certifications findById(int theId) {
		
		Optional<Certifications> result = certificationsRepository.findById(theId);
		Certifications theCertifications = null;
		
		if (result.isPresent()) {
			
			theCertifications = result.get();
		}
		else {
			
			// we didn't find the employee
			throw new RuntimeException("Did not find theCertifications id - " + theId);
		}
		
		return theCertifications;
	}

	@Override
	public void save(Certifications theCertifications) {

		certificationsRepository.save(theCertifications);
	}

	@Override
	public void deleteById(int theId) {

		certificationsRepository.deleteById(theId);
	}
	
	@Override
	public List<Certifications> searchByName(String nameToSearch) {
		
		return certificationsRepository.searchByName(nameToSearch);
	}
	
	@Override
	public List<Certifications> searchCertificationsNoValidityLimitation() {
		
		return certificationsRepository.searchCertificationsNoValidityLimitation();
	}
	
	@Override
	public List<Certifications> searchCertificationsWithValidityLimitation() {
		
		return certificationsRepository.searchCertificationsWithValidityLimitation();		
	}
	
	@Override
	public long countAllCertifications() {
		
		return certificationsRepository.count();
	}


}
