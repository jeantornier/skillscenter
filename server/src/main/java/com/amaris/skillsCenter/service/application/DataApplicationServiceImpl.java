package com.amaris.skillsCenter.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amaris.skillsCenter.model.application.DataApplication;
import com.amaris.skillsCenter.repository.application.DataApplicationRepository;

@Service
public class DataApplicationServiceImpl implements DataApplicationService {

	@Autowired
	private DataApplicationRepository dataApplicationRepository;
	
	@Override
	public DataApplication getAllApplicationData() {

		return dataApplicationRepository.getAllApplicationData();
	}

	@Override
	public String getApplicationName() {

		return dataApplicationRepository.getApplicationName();
	}

	@Override
	public String getApplicationComments() {
		
		return dataApplicationRepository.getApplicationComments();
	}

	@Override
	public String getApplicationVersion() {

		/* Add the keyword '.SNAPSHOT' only in case the application is in developmnet */
		String version = dataApplicationRepository.getApplicationVersionMajor() + "." + dataApplicationRepository.getApplicationVersionMinor() + "." + dataApplicationRepository.getApplicationVersionPatch();
		
		if (isApplicationInDevelopment() == true) {
			version = version + ".SNAPSHOT";
		}
		
		return version;
	}

	@Override
	public String getApplicationVersionMajor() {
		
		return dataApplicationRepository.getApplicationVersionMajor();
	}

	@Override
	public String getApplicationVersionMinor() {
		
		return dataApplicationRepository.getApplicationVersionMinor();
	}

	@Override
	public String getApplicationVersionPatch() {

		return dataApplicationRepository.getApplicationVersionPatch();
	}

	@Override
	public boolean isApplicationInDevelopment() {
		
		if (dataApplicationRepository.applicationInDevelopment().toLowerCase().equals("true")) {
			return true;
		}

		return false;
	}
}
