package com.amaris.skillsCenter.service.application;

import java.util.List;

import com.amaris.skillsCenter.model.application.Skills;

public interface SkillsService {
	
	/* Find methods */
	public List<Skills> findAll();
	public Skills findById(int theId);
	
	/* Save and Delete methods */
	public void save(Skills theSkills);	
	public void deleteById(int theId);
}
