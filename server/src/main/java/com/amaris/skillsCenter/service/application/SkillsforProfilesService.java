package com.amaris.skillsCenter.service.application;

import java.util.List;

import com.amaris.skillsCenter.model.application.SkillsforProfiles;

public interface SkillsforProfilesService {

	/* Find methods */
	public List<SkillsforProfiles> findAll();
	public SkillsforProfiles findById(int theId);
	
	/* Save and Delete methods */
	public void save(SkillsforProfiles theSkillsforProfiles);	
	public void deleteById(int theId);
}
