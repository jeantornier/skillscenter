package com.amaris.skillsCenter.service.application;

import java.util.List;

import com.amaris.skillsCenter.model.application.Certifiers;

public interface CertifiersService {

	/* Exists methods */
	public boolean existsById(int theId);
	
	/* Find methods */
	public List<Certifiers> findAll();
	public Certifiers findById(int theId);
	
	/* Save and Delete methods */
	public void save(Certifiers theCertifiers);	
	public void deleteById(int theId);
	
	/* Search methods */
	public List<Certifiers> searchByName(String nameToSearch);
	
	/* Count methods */
	public long countAllCertifiers();
}
