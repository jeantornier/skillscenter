package com.amaris.skillsCenter.model.security;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
* The Users class is the model representing the table 'users' in the database.
* Each Users object represents the account of a user for the application 'Skills Center'.
*
* @author  Jean TORNIER
* @version 0.1.0
* @since   2020-01-14
*/
@Entity
@Table(name="users")
public class Users {
	
	/* Fields */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="username")
	private String userName;
	
	@Column(name="password")
	private String password;
	
	@Column(name="enabled")
	private Integer enabled;
	
	@ManyToMany(fetch=FetchType.LAZY,
				cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						  CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(name="users_authorities",
				joinColumns=@JoinColumn(name="users_id"),
				inverseJoinColumns=@JoinColumn(name="authorities_id"))
	@JsonIgnore
	private List<Authorities> authorities;

	/* Constructors */
	public Users() {
	}

	public Users(String userName, String password, Integer enabled) {
		this.userName = userName;
		this.password = password;
		this.enabled = enabled;
	}

	/* Getters / Setters */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public List<Authorities> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<Authorities> authorities) {
		this.authorities = authorities;
	}

	/* toString */
	@Override
	public String toString() {
		
		String message = "[" + id + "] -- Object Users --\n" + 
				"\tuserName : " + userName + "\n" + 
				"\tpassword : " + password + "\n" + 
				"\tenabled : " +  enabled + "\n" + 
				"\tList of the authorities : " +  authorities.size() + "\n"; 
		
		for (Authorities authority: getAuthorities()) {
			message = message + "\t\t- authority : " + authority.getAuthority() + "\n";
		}
		
		message = message + "[" + id + "] -- Object Users -- ";
		return message;
	}

	/* hashCode AND equals */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
		result = prime * result + id;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		if (enabled == null) {
			if (other.enabled != null)
				return false;
		} else if (!enabled.equals(other.enabled))
			return false;
		if (id != other.id)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}
	
	/**
	* 'addAuthority' is a method allowing to add authority to the User designed.
	* If the authority, to add as input, is already set for the User, then it will be ignored.
	* 
	* @param Authorities theAuthorityToAdd.
	* @return void.
	*/
	public void addAuthority(Authorities theAuthorityToAdd) {
		
		if (getAuthorities() == null) {
			authorities = new ArrayList<Authorities>();
		}
		
		if(!authorities.contains(theAuthorityToAdd)) { 
			authorities.add(theAuthorityToAdd);
		}
	}

	/**
	* 'addAuthorities' is a method allowing to add multiple authorities to the User designed.
	* If an authority, to add as input, is already set for the User, then it will be ignored.
	* 
	* @param List<Authorities> theAuthoritiesToAdd.
	* @return void.
	*/
	public void addAuthorities(List<Authorities> theAuthoritiesToAdd) {
	
		if (getAuthorities() == null) {
			authorities = new ArrayList<Authorities>();
		}
		
		for (Authorities theAuthorityToAdd: theAuthoritiesToAdd) {
			
			if(!authorities.contains(theAuthorityToAdd)) { 
				authorities.add(theAuthorityToAdd);
			}
		}
	}
	
	/**
	* 'removeAuthority' is a method allowing to remove an authority to the User designed.
	* If the authority, to remove as input, isn't set for the User, then it will be ignored.
	* 
	* @param Authorities theAuthorityToRemove.
	* @return void.
	*/
	public void removeAuthority(Authorities theAuthorityToRemove) {
		
		if (getAuthorities() == null) {
			authorities = new ArrayList<Authorities>();
		}
		else if(authorities.contains(theAuthorityToRemove)) { 
			authorities.remove(theAuthorityToRemove);
		}
	}
	
	/**
	* 'removeAuthorities' is a method allowing to remove multiple authorities to the User designed.
	* If an authority, to remove as input, isn't set for the User, then it will be ignored.
	* 
	* @param List<Authorities> theAuthoritiesToRemove.
	* @return void.
	*/
	public void removeAuthorities(List<Authorities> theAuthoritiesToRemove) {
	
		if (getAuthorities() == null) {
			authorities = new ArrayList<Authorities>();
		} else { 
		
			for (Authorities theAuthorityToRemove: theAuthoritiesToRemove) {
				
				if(authorities.contains(theAuthorityToRemove)) { 
					authorities.remove(theAuthorityToRemove);
				}
			}
		}
	}
	
	/**
	* 'purgeAuthorities' is a method allowing to remove all the authorities to the User designed.
	* 
	* @param no arguments.
	* @return void.
	*/
	public void purgeAuthorities() {
		
		authorities = new ArrayList<Authorities>();
	}
}
