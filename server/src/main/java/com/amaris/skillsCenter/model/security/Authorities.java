package com.amaris.skillsCenter.model.security;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="authorities")
public class Authorities {
		
	/* Fields */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="authority")
	private String authority;
	
	@ManyToMany(fetch=FetchType.LAZY,
				cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						  CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(name="users_authorities",
				joinColumns=@JoinColumn(name="authorities_id"),
				inverseJoinColumns=@JoinColumn(name="users_id"))
	private List<Users> users;
	
	@ManyToMany(fetch=FetchType.LAZY,
				cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						  CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(name="authorities_users_requests",
				joinColumns=@JoinColumn(name="authorities_id"),
				inverseJoinColumns=@JoinColumn(name="users_requests_id"))
	@JsonIgnore
	private List<UsersRequests> usersRequests;

	/* Constructors */
	public Authorities() {
	}

	public Authorities(String authority) {
		this.authority = authority;
	}
	
	/* Getters / Setters */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}

	public List<UsersRequests> getUsersRequests() {
		return usersRequests;
	}

	public void setUsersRequests(List<UsersRequests> usersRequests) {
		this.usersRequests = usersRequests;
	}

	/* toString */
	@Override
	public String toString() {
		return "Authorities [id=" + id + ", authority=" + authority + "]";
	}

	/* hashCode AND equals */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authority == null) ? 0 : authority.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Authorities other = (Authorities) obj;
		if (authority == null) {
			if (other.authority != null)
				return false;
		} else if (!authority.equals(other.authority))
			return false;
		if (id != other.id)
			return false;
		return true;
	}
	
	/**
	* 'addUser' is a method allowing to add Users to the Authority designed.
	* If the User, to add as input, is already set for the Authority, then it will be ignored.
	* 
	* @param Users theUserToAdd.
	* @return void.
	*/
	public void addUser(Users theUserToAdd) {
		
		if (getUsers() == null) {
			setUsers(new ArrayList<Users>());
		}
		
		if(!getUsers().contains(theUserToAdd)) { 
			getUsers().add(theUserToAdd);
		}
	}

	/**
	* 'addUsers' is a method allowing to add multiple users to the Authority designed.
	* If a user, to add as input, is already set for the Authority, then it will be ignored.
	* 
	* @param List<Users> theUsersToAdd.
	* @return void.
	*/
	public void addUsers(List<Users> theUsersToAdd) {
	
		if (getUsers() == null) {
			setUsers(new ArrayList<Users>());
		}
		
		for (Users theUserToAdd: theUsersToAdd) {
			
			if(!getUsers().contains(theUserToAdd)) { 
				getUsers().add(theUserToAdd);
			}
		}
	}
	
	/**
	* 'addUserRequest' is a method allowing to add User Request to the Authority designed.
	* If the User Request, to add as input, is already set for the Authority, then it will be ignored.
	* 
	* @param UsersRequests theUserRequestToAdd.
	* @return void.
	*/
	public void addUser(UsersRequests theUserRequestToAdd) {
		
		if (getUsersRequests() == null) {
			setUsersRequests(new ArrayList<UsersRequests>());
		}
		
		if(!getUsersRequests().contains(theUserRequestToAdd)) { 
			getUsersRequests().add(theUserRequestToAdd);
		}
	}

	/**
	* 'addUsersRequests' is a method allowing to add multiple users requests to the Authority designed.
	* If a user request, to add as input, is already set for the Authority, then it will be ignored.
	* 
	* @param List<UsersRequests> theUsersRequestsToAdd.
	* @return void.
	*/
	public void addUsersRequests(List<UsersRequests> theUsersRequestsToAdd) {
	
		if (getUsersRequests() == null) {
			setUsersRequests(new ArrayList<UsersRequests>());
		}
		
		for (UsersRequests theUserRequestToAdd: theUsersRequestsToAdd) {
			
			if(!getUsersRequests().contains(theUserRequestToAdd)) { 
				getUsersRequests().add(theUserRequestToAdd);
			}
		}
	}
	
	/**
	* 'removeUser' is a method allowing to remove an user to the Authority designed.
	* If the user, to remove as input, isn't set for the Authority, then it will be ignored.
	* 
	* @param Users theUserToRemove.
	* @return void.
	*/
	public void removeUser(Users theUserToRemove) {
		
		if (getUsers() == null) {
			setUsers(new ArrayList<Users>());
		}
		else if(getUsers().contains(theUserToRemove)) { 
			getUsers().remove(theUserToRemove);
		}
	}
	
	/**
	* 'removeUsers' is a method allowing to remove multiple users to the Authority designed.
	* If a user, to remove as input, isn't set for the Authority, then it will be ignored.
	* 
	* @param List<Users> theUsersToRemove.
	* @return void.
	*/
	public void removeUsers(List<Users> theUsersToRemove) {
	
		if (getUsers() == null) {
			setUsers(new ArrayList<Users>());
			
		} else { 
		
			for (Users theUserToRemove: theUsersToRemove) {
				
				if(getUsers().contains(theUserToRemove)) { 
					getUsers().remove(theUserToRemove);
				}
			}
		}
	}
	
	/**
	* 'removeUserRequest' is a method allowing to remove a user request for an Authority designed.
	* If the user request, to remove as input, isn't set for the Authority, then it will be ignored.
	* 
	* @param UsersRequests theUserRequestToRemove.
	* @return void.
	*/
	public void removeUserRequest(UsersRequests theUserRequestToRemove) {
		
		if (getUsersRequests() == null) {
			setUsersRequests(new ArrayList<UsersRequests>());
		}
		else if(getUsersRequests().contains(theUserRequestToRemove)) { 
			getUsersRequests().remove(theUserRequestToRemove);
		}
	}
	
	/**
	* 'removeUsersRequests' is a method allowing to remove multiple users requests to the Authority designed.
	* If a user request, to remove as input, isn't set for the Authority, then it will be ignored.
	* 
	* @param List<UsersRequests> theUsersRequestsToRemove.
	* @return void.
	*/
	public void removeUsersRequests(List<UsersRequests> theUsersRequestsToRemove) {
	
		if (getUsersRequests() == null) {
			setUsersRequests(new ArrayList<UsersRequests>());
			
		} else { 
		
			for (UsersRequests theUserRequestToRemove: theUsersRequestsToRemove) {
				
				if(getUsersRequests().contains(theUserRequestToRemove)) { 
					getUsersRequests().remove(theUserRequestToRemove);
				}
			}
		}
	}
	
	/* Custom methods */
	public static boolean isAuthoritySuperAdmin(String superAdminLabel, Authorities theAuthority) { 
				
		String authorityLabel = theAuthority.getAuthority().trim().toLowerCase();
		superAdminLabel = superAdminLabel.trim().toLowerCase();
				
		if (authorityLabel.equals(superAdminLabel)) { 
			return true;
		}
		
		return false;
	}
}
