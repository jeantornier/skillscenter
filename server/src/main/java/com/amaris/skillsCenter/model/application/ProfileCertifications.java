package com.amaris.skillsCenter.model.application;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="profiles_certifications")
public class ProfileCertifications {

	/* Fields */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="certification_date")
	private Date certificationDate;
	
	@Column(name="comments")
	private String comments;
	
	@OneToOne(cascade= {CascadeType.DETACH, CascadeType.PERSIST,
						CascadeType.MERGE, CascadeType.REFRESH}, 
			  fetch = FetchType.EAGER)
	@JoinColumn(name="certifications_id")
	private Certifications certification;

	@OneToOne(cascade= {CascadeType.DETACH, CascadeType.PERSIST,
						CascadeType.MERGE, CascadeType.REFRESH}, 
			  fetch = FetchType.EAGER)
	@JoinColumn(name="profiles_id")
	private Profiles profile;

	/* Constructors */
	public ProfileCertifications() {
	}

	public ProfileCertifications(Date certificationDate, String comments) {
		this.certificationDate = certificationDate;
		this.comments = comments;
	}

	/* Getters / Setters */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCertificationDate() {
		return certificationDate;
	}

	public void setCertificationDate(Date certificationDate) {
		this.certificationDate = certificationDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Certifications getCertification() {
		return certification;
	}

	public void setCertification(Certifications certification) {
		this.certification = certification;
	}

	public Profiles getProfile() {
		return profile;
	}

	public void setProfile(Profiles profile) {
		this.profile = profile;
	}

	/* toString */
	@Override
	public String toString() {
		return "ProfileCertifications [id=" + id + ", certificationDate=" + certificationDate + ", comments=" + comments
				+ ", certification=" + certification + ", profile=" + profile + "]";
	}
}
