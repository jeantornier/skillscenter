package com.amaris.skillsCenter.model.application;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="profiles_skills")
public class SkillsforProfiles {

	/* Fields */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="interest_rate")
	private Integer interestRate;
	
	@Column(name="management_rate")
	private Integer managementRate;
	
	@Column(name="experience_months")
	private Integer experienceMonths;
	
	@Column(name="comments")
	private String comments;
	
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.PERSIST,
						CascadeType.MERGE, CascadeType.REFRESH}, 
			  fetch = FetchType.EAGER)
	@JoinColumn(name="profiles_id")
	private Profiles profile;
	
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.PERSIST,
						CascadeType.MERGE, CascadeType.REFRESH}, 
			  fetch = FetchType.EAGER)
	@JoinColumn(name="skills_id")
	private Skills skill;

	/* Constructors */
	public SkillsforProfiles() {
	}

	public SkillsforProfiles(Integer interestRate, Integer managementRate, Integer experienceMonths, String comments) {
		this.interestRate = interestRate;
		this.managementRate = managementRate;
		this.experienceMonths = experienceMonths;
		this.comments = comments;
	}

	/* Getters / Setters */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Integer interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getManagementRate() {
		return managementRate;
	}

	public void setManagementRate(Integer managementRate) {
		this.managementRate = managementRate;
	}

	public Integer getExperienceMonths() {
		return experienceMonths;
	}

	public void setExperienceMonths(Integer experienceMonths) {
		this.experienceMonths = experienceMonths;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Profiles getProfile() {
		return profile;
	}

	public void setProfile(Profiles profile) {
		this.profile = profile;
	}

	public Skills getSkill() {
		return skill;
	}

	public void setSkill(Skills skill) {
		this.skill = skill;
	}

	/* toString */
	@Override
	public String toString() {
		return "SkillsforProfiles [id=" + id + ", interestRate=" + interestRate + ", managementRate=" + managementRate
				+ ", experienceMonths=" + experienceMonths + ", comments=" + comments + ", profile=" + profile
				+ ", skill=" + skill + "]";
	}
}
