package com.amaris.skillsCenter.model.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DataApplication {

	/* The content of this object is made by injection of properties from the file 'DataApplication.properties' 
	 * The file is saved in the /resources folder of the server. */
	
	/* Fields */
	@Value("${application.name}")
	private String applicationName;
	
	@Value("${application.development}")
	private String development; // service layer convert the String into a boolean value

	@Value("${application.version.major}")
	private String versionMajor;
	@Value("${application.version.minor}")
	private String versionMinor;
	@Value("${application.version.patch}")
	private String versionPatch;
	
	@Value("${application.comments}")
	private String comments;

	/* Constructors */
	public DataApplication() {
	}

	public DataApplication(String applicationName, String development, String versionMajor, String versionMinor,
			String versionPatch, String comments) {
		this.applicationName = applicationName;
		this.development = development;
		this.versionMajor = versionMajor;
		this.versionMinor = versionMinor;
		this.versionPatch = versionPatch;
		this.comments = comments;
	}

	/* Getters / Setters */
	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getDevelopment() {
		return development;
	}

	public void setDevelopment(String development) {
		this.development = development;
	}

	public String getVersionMajor() {
		return versionMajor;
	}

	public void setVersionMajor(String versionMajor) {
		this.versionMajor = versionMajor;
	}

	public String getVersionMinor() {
		return versionMinor;
	}

	public void setVersionMinor(String versionMinor) {
		this.versionMinor = versionMinor;
	}

	public String getVersionPatch() {
		return versionPatch;
	}

	public void setVersionPatch(String versionPatch) {
		this.versionPatch = versionPatch;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/* toString method */
	@Override
	public String toString() {
		return "DataApplication [applicationName=" + applicationName + ", isInDevelopment=" + development
				+ ", versionMajor=" + versionMajor + ", versionMinor=" + versionMinor + ", versionPatch=" + versionPatch
				+ ", comments=" + comments + "]";
	}

}
