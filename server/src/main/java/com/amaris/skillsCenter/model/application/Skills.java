package com.amaris.skillsCenter.model.application;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="skills")
public class Skills {

	/* Fields */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;
	
	@ManyToMany(fetch=FetchType.LAZY,
				cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						  CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(name="skills_sections",
				joinColumns=@JoinColumn(name="skills_id"),
				inverseJoinColumns=@JoinColumn(name="sections_id"))
	@JsonIgnore
	private List<Sections> sections;
	
	@OneToMany(cascade= {CascadeType.DETACH, CascadeType.PERSIST,
						 CascadeType.MERGE, CascadeType.REFRESH}, 
			   fetch = FetchType.LAZY,
			   mappedBy="skill")
	@JsonIgnore
	private List<SkillsforProfiles> profilesInteresedBy;

	/* constructors */
	public Skills() {
	}

	public Skills(String name, String description) {
		this.name = name;
		this.description = description;
	}

	/* Getters / Setters */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Sections> getSections() {
		return sections;
	}

	public void setSections(List<Sections> sections) {
		this.sections = sections;
	}

	public List<SkillsforProfiles> getProfilesInteresedBy() {
		return profilesInteresedBy;
	}

	public void setProfilesInteresedBy(List<SkillsforProfiles> profilesInteresedBy) {
		this.profilesInteresedBy = profilesInteresedBy;
	}

	/* toString */
	@Override
	public String toString() {
		return "Skills [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
}
