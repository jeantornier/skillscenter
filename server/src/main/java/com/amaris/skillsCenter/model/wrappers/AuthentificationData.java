package com.amaris.skillsCenter.model.wrappers;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class AuthentificationData {
	
	private static final String bcryptHeader = "{bcrypt}";
	private static final int bcryptPasswordSize = 10;
	
	/* Fields */
	private String username;
	
	private String password;

	/* Constructors */
	public AuthentificationData() {
	}

	public AuthentificationData(String username, String password) {
		this.username = username.trim();
		this.password = password.trim();
	}

	/* Getters / Setters */
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password.trim();
	}

	public static int getBcryptPasswordSize() {
		return bcryptPasswordSize;
	}

	public static String getBcryptheader() {
		return bcryptHeader;
	}

	/* toString */
	@Override
	public String toString() {
		return "AuthentificationRequest [username='" + username + "', password='" + password + "']";
	}
	
	/* Bcrypt methods */
	public static String hashPassword(String password) {
		
		/* if the password starts with bcrypt header -> don't hash it again because it is already encrypted */
		if (password.indexOf(AuthentificationData.getBcryptheader()) == 0) { 
			return password;
		}
		return AuthentificationData.getBcryptheader() + BCrypt.hashpw(password, BCrypt.gensalt(AuthentificationData.getBcryptPasswordSize()));				
    }
	
	public boolean comparePasswords(String hashedPassword) {
		
		/* remove the header for SQL */
		if (hashedPassword.indexOf(AuthentificationData.getBcryptheader()) == 0) {
			hashedPassword = hashedPassword.substring(AuthentificationData.getBcryptheader().length());
		}
		
		return BCrypt.checkpw(getPassword(), hashedPassword);
    }
	
	public static boolean comparePasswords(String hashedPassword_1, String hashedPassword_2) {
		
		/* remove the header for SQL */
		if (hashedPassword_1.indexOf(AuthentificationData.getBcryptheader()) == 0) {
			hashedPassword_1 = hashedPassword_1.substring(AuthentificationData.getBcryptheader().length());
		}
		
		if (hashedPassword_2.indexOf(AuthentificationData.getBcryptheader()) == 0) {
			hashedPassword_2 = hashedPassword_2.substring(AuthentificationData.getBcryptheader().length());
		}

		return BCrypt.checkpw(hashedPassword_1, hashedPassword_2);
    }
	
}
