package com.amaris.skillsCenter.model.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="users_requests")
public class UsersRequests {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@Column(name="username")
	private String userName;
	
	@Column(name="password")
	private String password;
	
	@Column(name="enabled")
	private Integer enabled;
	
	@Column(name="request_date")
	private Date requestDate;
	
	@ManyToMany(fetch=FetchType.LAZY,
				cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						  CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(name="authorities_users_requests",
				joinColumns=@JoinColumn(name="users_requests_id"),
				inverseJoinColumns=@JoinColumn(name="authorities_id"))
	private List<Authorities> authorities;
	
	@OneToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
	  					CascadeType.DETACH, CascadeType.REFRESH}, 
			  fetch = FetchType.EAGER)
	@JoinColumn(name="users_id")
	private Users userRequester;

	/* Constructors */
	public UsersRequests() {
	}

	public UsersRequests(String userName, String password, Integer enabled, Date requestDate) {
		this.userName = userName;
		this.password = password;
		this.enabled = enabled;
		this.requestDate = requestDate;
	}

	/* Getters / Setters */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public List<Authorities> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<Authorities> authorities) {
		this.authorities = authorities;
	}

	public Users getUserRequester() {
		return userRequester;
	}

	public void setUserRequester(Users userRequester) {
		this.userRequester = userRequester;
	}

	/* toString */
	@Override
	public String toString() {
		return "UsersRequests [id=" + id + ", userName=" + userName + ", password=" + password + ", enabled=" + enabled
				+ ", requestDate=" + requestDate + ", userRequester=" + userRequester + "]";
	}
	
	/* Custom methods */
	/**
	* 'addAuthority' is a method allowing to add an Authority to the User Request designed.
	* If the Authority, to add as input, is already set for the User Request, then it will be ignored.
	* 
	* @param UsersRequests theUserRequestToAdd.
	* @return void.
	*/
	public void addAuthority(Authorities theAuthorityToAdd) {
		
		if (getAuthorities() == null) {
			setAuthorities(new ArrayList<Authorities>());
		}
		
		if(!getAuthorities().contains(theAuthorityToAdd)) { 
			getAuthorities().add(theAuthorityToAdd);
		}
	}

	/**
	* 'addAuthorities' is a method allowing to add multiple authorities to the User Request designed.
	* If an authority, to add as input, is already set for the User Request, then it will be ignored.
	* 
	* @param List<UsersRequests> theUsersRequestsToAdd.
	* @return void.
	*/
	public void addAuthorities(List<Authorities> theAuthoritiesToAdd) {
	
		if (getAuthorities() == null) {
			setAuthorities(new ArrayList<Authorities>());
		}
		
		for (Authorities theAuthorityToAdd: theAuthoritiesToAdd) {
			
			if(!getAuthorities().contains(theAuthorityToAdd)) { 
				getAuthorities().add(theAuthorityToAdd);
			}
		}
	}
	
	/**
	* 'removeAuthority' is a method allowing to remove an authority to the User Request designed.
	* If the authority, to remove as input, isn't set for the User Request, then it will be ignored.
	* 
	* @param Authorities theAuthorityToRemove.
	* @return void.
	*/
	public void removeAuthority(Authorities theAuthorityToRemove) {
		
		if (getAuthorities() == null) {
			setAuthorities(new ArrayList<Authorities>());
		}
		else if(getAuthorities().contains(theAuthorityToRemove)) { 
			getAuthorities().remove(theAuthorityToRemove);
		}
	}
	
	/**
	* 'removeAuthorities' is a method allowing to remove multiple authorities to the User Request designed.
	* If an authority, to remove as input, isn't set for the User Request, then it will be ignored.
	* 
	* @param List<Authorities> theAuthoritiesToRemove.
	* @return void.
	*/
	public void removeAuthorities(List<Authorities> theAuthoritiesToRemove) {
	
		if (getAuthorities() == null) {
			setAuthorities(new ArrayList<Authorities>());
			
		} else { 
		
			for (Authorities theAuthorityToRemove: theAuthoritiesToRemove) {
				
				if(getAuthorities().contains(theAuthorityToRemove)) { 
					getAuthorities().remove(theAuthorityToRemove);
				}
			}
		}
	}
}
