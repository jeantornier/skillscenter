package com.amaris.skillsCenter.model.application;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="profiles")
public class Profiles {

	/* Fields */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="email")
	private String email;
	
	@Column(name="title")
	private String title;
	
	@Column(name="birth_date")
	private Date birthDate;
	
	@Column(name="hired_date")
	private Date hiredDate;
	
	@Column(name="full_experience_months")
	private Integer fullExperienceMonths;
	
	@Column(name="is_manager")
	private Integer isManager;
	
	@Column(name="locked")
	private Integer isLocked;
	
	@OneToMany(mappedBy="profile",
				fetch=FetchType.LAZY,
				cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						  CascadeType.DETACH, CascadeType.REFRESH})
	@JsonIgnore
	private List<ProfileCertifications> profileCertifications;
	
	@OneToOne(cascade= {CascadeType.DETACH, CascadeType.PERSIST,
						CascadeType.MERGE, CascadeType.REFRESH}, 
			  fetch = FetchType.EAGER)
	@JoinColumn(name="departments_id")
	private Departments department;
	
	@OneToMany(cascade= {CascadeType.DETACH, CascadeType.PERSIST,
						 CascadeType.MERGE, CascadeType.REFRESH}, 
			   fetch = FetchType.LAZY,
			   mappedBy="profile")
	@JsonIgnore
	private List<SkillsforProfiles> skillsInterestedBy;

	/* Constructors */
	public Profiles() {
	}

	public Profiles(String firstName, String lastName, String email, String title, Date birthDate, Date hiredDate,
			Integer fullExperienceMonths, Integer isManager, Integer isLocked) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.title = title;
		this.birthDate = birthDate;
		this.hiredDate = hiredDate;
		this.fullExperienceMonths = fullExperienceMonths;
		this.isManager = isManager;
		this.isLocked = isLocked;
	}

	/* Getters / Setters */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHiredDate() {
		return hiredDate;
	}

	public void setHiredDate(Date hiredDate) {
		this.hiredDate = hiredDate;
	}

	public Integer getFullExperienceMonths() {
		return fullExperienceMonths;
	}

	public void setFullExperienceMonths(Integer fullExperienceMonths) {
		this.fullExperienceMonths = fullExperienceMonths;
	}

	public Integer getIsManager() {
		return isManager;
	}

	public void setIsManager(Integer isManager) {
		this.isManager = isManager;
	}

	public Integer getIsLocked() {
		return isLocked;
	}

	public void setIsLocked(Integer isLocked) {
		this.isLocked = isLocked;
	}

	public List<ProfileCertifications> getProfileCertifications() {
		return profileCertifications;
	}

	public void setProfileCertifications(List<ProfileCertifications> profileCertifications) {
		this.profileCertifications = profileCertifications;
	}

	public Departments getDepartment() {
		return department;
	}

	public void setDepartment(Departments department) {
		this.department = department;
	}

	public List<SkillsforProfiles> getSkillsInterestedBy() {
		return skillsInterestedBy;
	}

	public void setSkillsInterestedBy(List<SkillsforProfiles> skillsInterestedBy) {
		this.skillsInterestedBy = skillsInterestedBy;
	}

	/* toString */
	@Override
	public String toString() {
		return "Profiles [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", title=" + title + ", birthDate=" + birthDate + ", hiredDate=" + hiredDate + ", fullExperienceMonths="
				+ fullExperienceMonths + ", isManager=" + isManager + ", isLocked=" + isLocked + ", department=" + department + "]";
	}
}
