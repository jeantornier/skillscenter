package com.amaris.skillsCenter.model.application;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="certifications")
public class Certifications {

	/* Fields */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;
	
	@Column(name="validity_months")
	private Integer validityMonths;
	
	@ManyToMany(fetch=FetchType.LAZY,
				cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						  CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(name="certifications_certifiers",
				joinColumns=@JoinColumn(name="certifications_id"),
				inverseJoinColumns=@JoinColumn(name="certifiers_id"))
	@JsonIgnore
	private List<Certifiers> certifiers;
	
	@OneToMany(mappedBy="certification",
			fetch=FetchType.LAZY,
			cascade= {CascadeType.PERSIST, CascadeType.MERGE,
					  CascadeType.DETACH, CascadeType.REFRESH})
	@JsonIgnore
	private List<ProfileCertifications> profileCertifications;

	/* Constructors */
	public Certifications() {
	}

	public Certifications(String name, String description, Integer validityMonths) {
		this.name = name;
		this.description = description;
		this.validityMonths = validityMonths;
	}

	/* Getters / Setters */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getValidityMonths() {
		return validityMonths;
	}

	public void setValidityMonths(Integer validityMonths) {
		this.validityMonths = validityMonths;
	}

	public List<Certifiers> getCertifiers() {
		return certifiers;
	}

	public void setCertifiers(List<Certifiers> certifiers) {
		this.certifiers = certifiers;
	}

	public List<ProfileCertifications> getProfileCertifications() {
		return profileCertifications;
	}

	public void setProfileCertifications(List<ProfileCertifications> profileCertifications) {
		this.profileCertifications = profileCertifications;
	}

	/* toString */
	@Override
	public String toString() {
		return "Certifications [id=" + id + ", name=" + name + ", description=" + description + ", validityMonths="
				+ validityMonths + "]";
	}
}
