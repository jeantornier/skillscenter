package com.amaris.skillsCenter.model.wrappers;

import java.util.List;

public class UsersAuthorities {

	private Integer userId;
	
	private List<Integer> authoritiesIds;

	public UsersAuthorities() {
	}

	public UsersAuthorities(Integer userId, List<Integer> authoritiesIds) {
		this.userId = userId;
		this.authoritiesIds = authoritiesIds;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<Integer> getAuthoritiesIds() {
		return authoritiesIds;
	}

	public void setAuthoritiesIds(List<Integer> authoritiesIds) {
		this.authoritiesIds = authoritiesIds;
	}

	@Override
	public String toString() {
		return "UsersAuthorities [userId=" + userId + ", authoritiesIds=" + authoritiesIds + "]";
	}
}
