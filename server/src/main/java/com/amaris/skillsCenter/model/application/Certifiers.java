package com.amaris.skillsCenter.model.application;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="certifiers")
public class Certifiers {

	/* Fields */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;
	
	@ManyToMany(fetch=FetchType.LAZY,
				cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						  CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(name="certifications_certifiers",
				joinColumns=@JoinColumn(name="certifiers_id"),
				inverseJoinColumns=@JoinColumn(name="certifications_id"))
	@JsonIgnore
	private List<Certifications> certifications;

	/* Constructors */
	public Certifiers() {
	}

	public Certifiers(String name, String description) {
		this.name = name;
		this.description = description;
	}

	/* Getters / Setters */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Certifications> getCertifications() {
		return certifications;
	}

	public void setCertifications(List<Certifications> certifications) {
		this.certifications = certifications;
	}

	/* toString */
	@Override
	public String toString() {
		return "Certifiers [id=" + id + ", name=" + name + ", description=" + description + ", certifications="
				+ certifications + "]";
	}
}
