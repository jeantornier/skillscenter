package com.amaris.skillsCenter.restController.errorHandlers.Users;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UsersExceptionHandler {	
	
	@ExceptionHandler
	public ResponseEntity<UsersErrorResponse> handleException(UsersWrongPasswordException exc) {
		
		UsersErrorResponse error = new UsersErrorResponse();
		
		error.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_ACCEPTABLE);
	}
	
	@ExceptionHandler
	public ResponseEntity<UsersErrorResponse> handleException(UsersWrongUserNameException exc) {
		
		UsersErrorResponse error = new UsersErrorResponse();
		
		error.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_ACCEPTABLE);
	}
	
	
	@ExceptionHandler
	public ResponseEntity<UsersErrorResponse> handleException(UsersFoundException exc) {
		
		UsersErrorResponse error = new UsersErrorResponse();
		
		error.setStatus(HttpStatus.FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<UsersErrorResponse> handleException(UsersNotFoundException exc) {
		
		UsersErrorResponse error = new UsersErrorResponse();
		
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<UsersErrorResponse> handleException(UsersLockedException exc) {
		
		UsersErrorResponse error = new UsersErrorResponse();
		
		error.setStatus(HttpStatus.LOCKED.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.LOCKED);
	}
	
	public ResponseEntity<UsersErrorResponse> handleException(UsersAuthentificationFailureException exc) {
		
		UsersErrorResponse error = new UsersErrorResponse();
		
		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public ResponseEntity<UsersErrorResponse> handleException(UsersAuthentificationProcessFailureException exc) {
		
		UsersErrorResponse error = new UsersErrorResponse();
		
		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
		
	/* Handle all the exceptions */
	@ExceptionHandler
	public ResponseEntity<UsersErrorResponse> handleException(Exception exc) {
		
		UsersErrorResponse error = new UsersErrorResponse();
		
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}
