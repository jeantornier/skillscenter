package com.amaris.skillsCenter.restController.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.application.Sections;
import com.amaris.skillsCenter.service.application.SectionsService;

@RestController
@RequestMapping("/sections")
public class SectionsRestController {
	
	@Autowired
	private SectionsService sectionsService;
	
	@GetMapping("/all")
	public List<Sections> findAll() {
		return sectionsService.findAll();
	}
}
