package com.amaris.skillsCenter.restController.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.application.ProfileCertifications;
import com.amaris.skillsCenter.service.application.ProfileCertificationsService;

@RestController
@RequestMapping("/profileCertifications")
public class ProfileCertificationsRestController {
	
	@Autowired
	private ProfileCertificationsService profileCertificationsService;
	
	@GetMapping("/all")
	public List<ProfileCertifications> findAll() {
		return profileCertificationsService.findAll();
	}
}
