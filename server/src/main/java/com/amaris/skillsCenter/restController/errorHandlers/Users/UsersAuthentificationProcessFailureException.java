package com.amaris.skillsCenter.restController.errorHandlers.Users;

public class UsersAuthentificationProcessFailureException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersAuthentificationProcessFailureException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersAuthentificationProcessFailureException(String message) {
		super(message);
	}

	public UsersAuthentificationProcessFailureException(Throwable cause) {
		super(cause);
	}
}
