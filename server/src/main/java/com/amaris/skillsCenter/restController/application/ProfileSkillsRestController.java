package com.amaris.skillsCenter.restController.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.application.SkillsforProfiles;
import com.amaris.skillsCenter.service.application.SkillsforProfilesService;

@RestController
@RequestMapping("/profileSkills")
public class ProfileSkillsRestController {
	
	@Autowired
	private SkillsforProfilesService skillsForProfilesService;
	
	@GetMapping("/all")
	public List<SkillsforProfiles> findAll() {
		return skillsForProfilesService.findAll();
	}
}
