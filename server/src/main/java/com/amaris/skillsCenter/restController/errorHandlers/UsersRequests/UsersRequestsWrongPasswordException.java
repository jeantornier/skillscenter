package com.amaris.skillsCenter.restController.errorHandlers.UsersRequests;

public class UsersRequestsWrongPasswordException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersRequestsWrongPasswordException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersRequestsWrongPasswordException(String message) {
		super(message);
	}

	public UsersRequestsWrongPasswordException(Throwable cause) {
		super(cause);
	}
}
