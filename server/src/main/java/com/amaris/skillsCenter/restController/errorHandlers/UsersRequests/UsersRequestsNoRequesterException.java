package com.amaris.skillsCenter.restController.errorHandlers.UsersRequests;

public class UsersRequestsNoRequesterException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersRequestsNoRequesterException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersRequestsNoRequesterException(String message) {
		super(message);
	}

	public UsersRequestsNoRequesterException(Throwable cause) {
		super(cause);
	}
}
