package com.amaris.skillsCenter.restController.errorHandlers.UsersRequests;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UsersRequestsExceptionHandler {	
	
	@ExceptionHandler
	public ResponseEntity<UsersRequestsErrorResponse> handleException(UsersRequestsWrongUserNameException exc) {
		
		UsersRequestsErrorResponse error = new UsersRequestsErrorResponse();
		
		error.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_ACCEPTABLE);
	}
	
	@ExceptionHandler
	public ResponseEntity<UsersRequestsErrorResponse> handleException(UsersRequestsWrongPasswordException exc) {
		
		UsersRequestsErrorResponse error = new UsersRequestsErrorResponse();
		
		error.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_ACCEPTABLE);
	}
	
	@ExceptionHandler
	public ResponseEntity<UsersRequestsErrorResponse> handleException(UsersRequestsNotFoundException exc) {
		
		UsersRequestsErrorResponse error = new UsersRequestsErrorResponse();
		
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<UsersRequestsErrorResponse> handleException(UsersRequestsNoRequesterException exc) {
		
		UsersRequestsErrorResponse error = new UsersRequestsErrorResponse();
		
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<UsersRequestsErrorResponse> handleException(UsersRequestsNoAuthoritiesException exc) {
		
		UsersRequestsErrorResponse error = new UsersRequestsErrorResponse();
		
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<UsersRequestsErrorResponse> handleException(UsersRequestsFoundException exc) {
		
		UsersRequestsErrorResponse error = new UsersRequestsErrorResponse();
		
		error.setStatus(HttpStatus.FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.FOUND);
	}
	
	/* Handle all the exceptions */
	@ExceptionHandler
	public ResponseEntity<UsersRequestsErrorResponse> handleException(Exception exc) {
		
		UsersRequestsErrorResponse error = new UsersRequestsErrorResponse();
		
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}
