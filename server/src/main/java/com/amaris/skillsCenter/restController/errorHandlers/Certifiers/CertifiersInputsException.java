package com.amaris.skillsCenter.restController.errorHandlers.Certifiers;

public class CertifiersInputsException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CertifiersInputsException(String message, Throwable cause) {
		super(message, cause);
	}

	public CertifiersInputsException(String message) {
		super(message);
	}

	public CertifiersInputsException(Throwable cause) {
		super(cause);
	}
}
