package com.amaris.skillsCenter.restController.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.application.Certifications;
import com.amaris.skillsCenter.restController.errorHandlers.Certifications.CertificationsInputsException;
import com.amaris.skillsCenter.restController.errorHandlers.Certifications.CertificationsNotFoundException;
import com.amaris.skillsCenter.service.application.CertificationsService;

@RestController
@RequestMapping("/certifications")
public class CertificationsRestController {
	
	@Autowired
	private CertificationsService certificationsService;
	
	@GetMapping("/all")
	public List<Certifications> findAll() {
		return certificationsService.findAll();
	}
	
	@GetMapping("/{certificationId}")
	public Certifications findById(@PathVariable("certificationId") int theId) {
		
		/* will raise an exception only if 'theId' to find isn't in the database */
		if (!certificationsService.existsById(theId)) {
			throw new CertificationsNotFoundException("The Certification ID '" + theId + "' can't be found in DataBase !");
		}
		
		return certificationsService.findById(theId);
	}
	
	@PostMapping("")
	public Certifications createCertifications(@RequestBody Certifications theCertification) {
		
		certificationsService.save(theCertification);
		return theCertification;
	}
	
	@PutMapping("")
	public Certifications updateCertifications(@RequestBody Certifications theCertification) {
		
		/* will raise an exception only if 'theId' to find isn't in the database */
		if (!certificationsService.existsById(theCertification.getId())) {
			throw new CertificationsNotFoundException("The Certification ID '" + theCertification.getId() + "' can't be found in DataBase - Update impossible !");
		}
		
		certificationsService.save(theCertification);
		return theCertification;
	}
	
	@DeleteMapping("/{certificationId}")
	public Certifications deleteCertificationsById(@PathVariable("certificationId") int theId) {
		
		Certifications theCertificationToDelete = null;
		
		/* will raise an exception only if 'theId' to find isn't in the database */
		if (!certificationsService.existsById(theId)) {
			throw new CertificationsNotFoundException("The Certification ID '" + theId + "' can't be found in DataBase - DeleteById impossible !");
		}
		else {
			
			theCertificationToDelete = certificationsService.findById(theId);
			certificationsService.deleteById(theCertificationToDelete.getId());
		}
		
		return theCertificationToDelete;
	}
	
	/* class specific methods */
	@GetMapping("/search/{name}")
	private List<Certifications> searchCertificationsByName(@PathVariable("name") String nameToSearch) {
		
		List<Certifications> results = null;
		nameToSearch = nameToSearch.trim();
		
		if (nameToSearch.length() <= 0) {
			throw new CertificationsInputsException("Certification name to search, '" + nameToSearch + "' is empty !");
		}
		
		results = certificationsService.searchByName(nameToSearch);
		return results;
	}
	
	@GetMapping("/search/noValidity")
	private List<Certifications> searchCertificationsNoValidityLimitation() {
				
		return certificationsService.searchCertificationsNoValidityLimitation();
	}
	
	@GetMapping("/search/withValidity")
	private List<Certifications> searchCertificationsWithValidityLimitation() {
				
		return certificationsService.searchCertificationsWithValidityLimitation();
	}
	
	@GetMapping("/count")
	private long countCertifications() {
				
		return certificationsService.countAllCertifications();
	}
}
