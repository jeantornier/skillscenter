package com.amaris.skillsCenter.restController.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.application.Profiles;
import com.amaris.skillsCenter.service.application.ProfilesService;

@RestController
@RequestMapping("/profiles")
public class ProfilesRestController {
	
	@Autowired
	private ProfilesService profilesService;
	
	@GetMapping("/all")
	public List<Profiles> findAll() {
		return profilesService.findAll();
	}
}
