package com.amaris.skillsCenter.restController.errorHandlers.Certifiers;

public class CertifiersNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CertifiersNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CertifiersNotFoundException(String message) {
		super(message);
	}

	public CertifiersNotFoundException(Throwable cause) {
		super(cause);
	}
}
