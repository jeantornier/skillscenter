package com.amaris.skillsCenter.restController.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.application.DataApplication;
import com.amaris.skillsCenter.service.application.DataApplicationService;

@RestController
@RequestMapping("/application")
public class DataApplicationRestController {

	@Autowired
	private DataApplicationService dataApplicationService;
	
	@GetMapping("/all")
	public DataApplication getAllData() {
		return dataApplicationService.getAllApplicationData();
	}
	
	@GetMapping("/name")
	public String getApplicationName() {
		return dataApplicationService.getApplicationName();
	}
	
	@GetMapping("/version")
	public String getApplicationVersion() {
		return dataApplicationService.getApplicationVersion();
	}
	
	@GetMapping("/version/major")
	public String getApplicationVersionMajor() {
		return dataApplicationService.getApplicationVersionMajor();
	}
	
	@GetMapping("/version/minor")
	public String getApplicationVersionMinor() {
		return dataApplicationService.getApplicationVersionMinor();
	}
	
	@GetMapping("/version/patch")
	public String getApplicationVersionPatch() {
		return dataApplicationService.getApplicationVersionPatch();
	}
	
	@GetMapping("/comments")
	public String getApplicationComments() {
		return dataApplicationService.getApplicationComments();
	}
	
	@GetMapping("/isInDevelopment")
	public boolean isApplicationInDevelopment() {
		return dataApplicationService.isApplicationInDevelopment();
	}
}
