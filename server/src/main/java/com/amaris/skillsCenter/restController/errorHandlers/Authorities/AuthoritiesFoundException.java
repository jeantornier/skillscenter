package com.amaris.skillsCenter.restController.errorHandlers.Authorities;

public class AuthoritiesFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthoritiesFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public AuthoritiesFoundException(String message) {
		super(message);
	}

	public AuthoritiesFoundException(Throwable cause) {
		super(cause);
	}
}
