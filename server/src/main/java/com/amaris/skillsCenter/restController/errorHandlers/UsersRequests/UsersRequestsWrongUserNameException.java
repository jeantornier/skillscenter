package com.amaris.skillsCenter.restController.errorHandlers.UsersRequests;

public class UsersRequestsWrongUserNameException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersRequestsWrongUserNameException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersRequestsWrongUserNameException(String message) {
		super(message);
	}

	public UsersRequestsWrongUserNameException(Throwable cause) {
		super(cause);
	}
}
