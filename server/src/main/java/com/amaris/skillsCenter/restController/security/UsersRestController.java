package com.amaris.skillsCenter.restController.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.model.wrappers.AuthentificationData;
import com.amaris.skillsCenter.model.wrappers.UsersAuthorities;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersNotFoundException;
import com.amaris.skillsCenter.service.security.UsersService;

@RestController
@RequestMapping("/users")
public class UsersRestController {
	
	@Autowired
	private UsersService usersService;
	
	/* ** ** ** ** ** 
	 * Find methods 
	 * ** ** ** ** ** */
	@GetMapping("/id/{userId}")
	public Users getByUserId(@PathVariable("userId") int theId) {
		return usersService.findById(theId);
	}
	
	@GetMapping("/name/{userName}")
	public Users getByUserName(@PathVariable("userName") String username) {
		return usersService.findByUserName(username);
	}
	
	@GetMapping("/superadmin")
	public List<Users> getSuperAdminUser() {
		return usersService.findUsersSuperAdmin();
	}
	
	@GetMapping("/disabled")
	public List<Users> findAllUsersDisabled() {
		return usersService.findAllUsersDisabled();
	}
	
	@GetMapping("/enabled")
	public List<Users> findAllUsersEnabled() {
		return usersService.findAllUsersEnabled();
	}

	/* ** ** ** ** ** 
	 * CUD methods 
	 * ** ** ** ** ** */
	@PostMapping("")
	public Users createUsers(@RequestBody Users theUser) throws UsersNotFoundException, UsersFoundException {
		
		/* will raise an exception only if 'username' to find is in the database */
		try { 
			usersService.findByUserName(theUser.getUserName());
			throw new UsersFoundException("The Username '" + theUser.getUserName() + "' is found in DataBase - Create impossible !");
		} catch (UsersNotFoundException exc) {
			usersService.createUser(theUser);
		}

		return theUser;
	}
	
	@PutMapping("")
	public Users updateUsers(@RequestBody Users theUser) throws UsersNotFoundException {
		
		/* will raise an exception only if 'theId' to find isn't in the database */
		if (!usersService.existsById(theUser.getId())) {
			throw new UsersNotFoundException("The User ID '" + theUser.getId() + "' can't be found in DataBase - Update impossible !");
		}
		
		usersService.updateUser(theUser, true);
		return theUser;
	}	
	
	@DeleteMapping("/{userId}")
	public Users deleteUsersById(@PathVariable("userId") int theId) {
		
		Users theUsersToDelete = null;
		
		try{ 
			theUsersToDelete = usersService.findById(theId);
			usersService.deleteById(theUsersToDelete.getId());
		} catch (UsersNotFoundException exc) {
			throw new UsersNotFoundException("The User ID '" + theId + "' can't be found in DataBase - DeleteById impossible !");

		}
		
		return theUsersToDelete;
	}
	
	/* ** ** ** ** ** 
	 * Specific methods 
	 * ** ** ** ** ** */
	@PostMapping("/authentificateTheUser")
	public Users authentificateTheUser(@RequestBody AuthentificationData data) {
		return usersService.authentification(data);
	}
	
	@PostMapping("/disable")
	public Users disableTheUser(@RequestBody Users theUserToDisable) {
		return usersService.disableTheUser(theUserToDisable);
	}
	
	@PostMapping("/enable")
	public Users enableTheUser(@RequestBody Users theUserToEnable) {
		return usersService.enableTheUser(theUserToEnable);
	}
	
	@PutMapping("/addAuthorities")
	public List<Authorities> addAuthority(@RequestBody UsersAuthorities usersAuthorities) {
		return usersService.addAuthorities(usersAuthorities);
	}
	
	@PutMapping("/removeAuthorities")
	public List<Authorities> removeAuthority(@RequestBody UsersAuthorities usersAuthorities) {
		return usersService.removeAuthorities(usersAuthorities);
	}
	
	@PutMapping("/eraseAuthorities")
	public List<Authorities> eraseAuthority(@RequestBody UsersAuthorities usersAuthorities) {
		return usersService.eraseAuthorities(usersAuthorities);
	}
}
