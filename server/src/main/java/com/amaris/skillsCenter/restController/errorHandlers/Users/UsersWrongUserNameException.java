package com.amaris.skillsCenter.restController.errorHandlers.Users;

public class UsersWrongUserNameException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersWrongUserNameException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersWrongUserNameException(String message) {
		super(message);
	}

	public UsersWrongUserNameException(Throwable cause) {
		super(cause);
	}
}
