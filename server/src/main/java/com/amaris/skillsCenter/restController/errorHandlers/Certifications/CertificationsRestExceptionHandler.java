package com.amaris.skillsCenter.restController.errorHandlers.Certifications;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CertificationsRestExceptionHandler {	
	
	@ExceptionHandler
	public ResponseEntity<CertificationsErrorResponse> handleException(CertificationsNotFoundException exc) {
				
		CertificationsErrorResponse error = new CertificationsErrorResponse();
		
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<CertificationsErrorResponse> handleException(CertificationsInputsException exc) {
				
		CertificationsErrorResponse error = new CertificationsErrorResponse();
		
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage("IO Exception on REST API, " + exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
		
	/* Handle all the exceptions */
	@ExceptionHandler
	public ResponseEntity<CertificationsErrorResponse> handleException(Exception exc) {
		
		CertificationsErrorResponse error = new CertificationsErrorResponse();
		
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}
