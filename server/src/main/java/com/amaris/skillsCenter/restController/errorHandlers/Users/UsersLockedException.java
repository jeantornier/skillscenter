package com.amaris.skillsCenter.restController.errorHandlers.Users;

public class UsersLockedException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersLockedException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersLockedException(String message) {
		super(message);
	}

	public UsersLockedException(Throwable cause) {
		super(cause);
	}
}
