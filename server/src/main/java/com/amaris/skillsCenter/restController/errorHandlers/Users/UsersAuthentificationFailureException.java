package com.amaris.skillsCenter.restController.errorHandlers.Users;

public class UsersAuthentificationFailureException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersAuthentificationFailureException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersAuthentificationFailureException(String message) {
		super(message);
	}

	public UsersAuthentificationFailureException(Throwable cause) {
		super(cause);
	}
}
