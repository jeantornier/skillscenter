package com.amaris.skillsCenter.restController.errorHandlers.Certifications;

public class CertificationsInputsException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CertificationsInputsException(String message, Throwable cause) {
		super(message, cause);
	}

	public CertificationsInputsException(String message) {
		super(message);
	}

	public CertificationsInputsException(Throwable cause) {
		super(cause);
	}
}
