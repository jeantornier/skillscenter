package com.amaris.skillsCenter.restController.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.restController.errorHandlers.Authorities.AuthoritiesFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.Authorities.AuthoritiesNotFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersNotFoundException;
import com.amaris.skillsCenter.service.security.AuthoritiesService;

@RestController
@RequestMapping("/authorities")
public class AuthoritiesRestController {

	@Autowired
	private AuthoritiesService authoritiesService;
	
	@GetMapping("/all")
	public List<Authorities> getAllAuthorities() {
		return authoritiesService.findAll();
	}
	
	@GetMapping("/{authorityId}")
	public Authorities getAuthorityById(@PathVariable("authorityId") int theId) throws AuthoritiesNotFoundException {
		return authoritiesService.findById(theId);
	}
	
	@GetMapping("/superAdministrator")
	public Authorities getSuperAdminAuthority() {
		return authoritiesService.findSuperAdminAuthority();
	}
	
	@PostMapping("")
	public Authorities createAuthority(@RequestBody Authorities theAuthorityToCreate) throws AuthoritiesNotFoundException, AuthoritiesFoundException, UsersNotFoundException {
		
		/* will raise an exception only if 'authorityLabel' to find is in the database */
		try { 
			authoritiesService.findByAuthority(theAuthorityToCreate.getAuthority());
			throw new AuthoritiesFoundException("The Authority Label '" + theAuthorityToCreate.getAuthority() + "' is found in DataBase - Create impossible !");
		} catch (AuthoritiesNotFoundException exc) {			
			authoritiesService.createAuthority(theAuthorityToCreate);
		}

		return theAuthorityToCreate;		
	}
	
	@PutMapping("")
	public Authorities updateAuthority(@RequestBody Authorities theAuthorityToUpdate) throws AuthoritiesFoundException {
		
		/* will raise an exception only if 'theId' to find isn't in the database */
		if (!authoritiesService.existsById(theAuthorityToUpdate.getId())) {
			throw new AuthoritiesFoundException("The Authority Id '" + theAuthorityToUpdate.getId() + "' isn't found in DataBase - Update impossible !");
		}
		
		authoritiesService.updateAuthority(theAuthorityToUpdate);
		return theAuthorityToUpdate;
	}
	
	@DeleteMapping("/{authorityId}")
	public Authorities deleteAuthorityById(@PathVariable("authorityId") int theId) throws AuthoritiesNotFoundException {

		Authorities theAuthorityToDelete = null;
		
		try{ 
			theAuthorityToDelete = authoritiesService.findById(theId);
			
			/* avoid making a delete with users set to null which cause an SQL error */
			if(theAuthorityToDelete.getUsers() == null || theAuthorityToDelete.getUsers().size() == 0) { 
				theAuthorityToDelete.setUsers(new ArrayList<Users>());
			}
			
			authoritiesService.deleteById(theAuthorityToDelete.getId());
		} catch (AuthoritiesNotFoundException exc) {
			throw new AuthoritiesFoundException("The Authority Id '" + theId + "' isn't found in DataBase - DeleteById impossible !");

		}
		
		return theAuthorityToDelete;
	}
}
