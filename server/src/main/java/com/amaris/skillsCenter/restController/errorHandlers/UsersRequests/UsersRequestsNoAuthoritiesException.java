package com.amaris.skillsCenter.restController.errorHandlers.UsersRequests;

public class UsersRequestsNoAuthoritiesException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersRequestsNoAuthoritiesException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersRequestsNoAuthoritiesException(String message) {
		super(message);
	}

	public UsersRequestsNoAuthoritiesException(Throwable cause) {
		super(cause);
	}
}
