package com.amaris.skillsCenter.restController.errorHandlers.UsersRequests;

public class UsersRequestsNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersRequestsNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersRequestsNotFoundException(String message) {
		super(message);
	}

	public UsersRequestsNotFoundException(Throwable cause) {
		super(cause);
	}
}
