package com.amaris.skillsCenter.restController.errorHandlers.Authorities;

public class AuthoritiesNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthoritiesNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public AuthoritiesNotFoundException(String message) {
		super(message);
	}

	public AuthoritiesNotFoundException(Throwable cause) {
		super(cause);
	}
}
