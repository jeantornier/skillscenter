package com.amaris.skillsCenter.restController.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.security.Authorities;
import com.amaris.skillsCenter.model.security.Users;
import com.amaris.skillsCenter.model.security.UsersRequests;
import com.amaris.skillsCenter.restController.errorHandlers.Authorities.AuthoritiesNotFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.Users.UsersNotFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.UsersRequests.UsersRequestsNotFoundException;
import com.amaris.skillsCenter.service.security.AuthoritiesService;
import com.amaris.skillsCenter.service.security.UsersRequestsService;
import com.amaris.skillsCenter.service.security.UsersService;

@RestController
@RequestMapping("/usersRequests")
public class UsersRequestsRestController {

	@Autowired
	private UsersRequestsService usersRequestsService;
	
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private AuthoritiesService authoritiesService;

	
	@GetMapping("/all")
	public List<UsersRequests> findAll() {
		return usersRequestsService.findAll();
	}
	
	@GetMapping("/{userRequestId}")
	public UsersRequests findByUserRequestId(@PathVariable("userRequestId") int theId) {
		return usersRequestsService.findById(theId);
	}
	
	@GetMapping("/username/{userRequestUserName}")
	public UsersRequests findByUserRequestUserName(@PathVariable("userRequestUserName") String username) {
		return usersRequestsService.findByUserName(username);
	}
	
	@GetMapping("/disabled")
	public List<UsersRequests> findByUserRequestDisabled() {
		return usersRequestsService.findAllRequestsDisabled();
	}
	
	@GetMapping("/enabled")
	public List<UsersRequests> findByUserRequestEnabled() {
		return usersRequestsService.findAllRequestsEnabled();
	}

	@GetMapping("/requester/id/{userRequesterId}")
	public List<UsersRequests> findByUserRequesterId(@PathVariable("userRequesterId") int userRequesterId) throws UsersNotFoundException {
		
		Users theUserRequester = usersService.findById(userRequesterId);
		return usersRequestsService.findAllRequestsByRequester(theUserRequester);
	}
	
	@GetMapping("/requester/userName/{userRequesterName}")
	public List<UsersRequests> findByUserRequesterUserName(@PathVariable("userRequesterName") String userRequesterUserName) throws UsersNotFoundException {
		
		Users theUserRequester = usersService.findByUserName(userRequesterUserName);
		return usersRequestsService.findAllRequestsByRequester(theUserRequester);
	}
	
	@GetMapping("/authority/id/{authorityId}")
	public List<UsersRequests> findByAuthorityId(@PathVariable("authorityId") int authorityId) throws AuthoritiesNotFoundException {
		
		Authorities theAuthority = authoritiesService.findById(authorityId);
		return usersRequestsService.findAllRequestsByAuthority(theAuthority);
	}
	
	@GetMapping("/authority/authorityLabel/{authorityLabel}")
	public List<UsersRequests> findByAuthorityLabel(@PathVariable("authorityLabel") String authorityLabel) throws AuthoritiesNotFoundException {
		
		Authorities theAuthority = authoritiesService.findByAuthority(authorityLabel);
		return usersRequestsService.findAllRequestsByAuthority(theAuthority);
	}
	
	@PostMapping("")
	public UsersRequests createUsersRequests(@RequestBody UsersRequests theUserRequestToAdd) throws UsersRequestsNotFoundException, UsersRequestsFoundException {
		
		/* will raise an exception only if 'id' to find is in the database */
		try { 
			UsersRequests theUserRequestFound = usersRequestsService.findUserRequest(theUserRequestToAdd);
			throw new UsersRequestsFoundException("The UsersRequests UserName '" + theUserRequestFound.getUserName() + "' is found in DataBase - Create impossible !");
		} catch (UsersRequestsNotFoundException exc) {
			usersRequestsService.createUserRequest(theUserRequestToAdd);
		}
		
		return theUserRequestToAdd;
	}
	
	@PutMapping("")
	public UsersRequests updateUsersRequests(@RequestBody UsersRequests theUserRequestToUpdate) throws UsersRequestsNotFoundException {
				
		/* will raise an exception only if 'id' to find is in the database */
		try { 
			UsersRequests oldUserRequestFound = usersRequestsService.findUserRequest(theUserRequestToUpdate);
			usersRequestsService.updateUserRequest(theUserRequestToUpdate, oldUserRequestFound);
		} catch (UsersRequestsNotFoundException exc) {
			throw new UsersRequestsNotFoundException("The User Request for '" + theUserRequestToUpdate.getUserName() + "' can't be found in DataBase - Update impossible !");
		}
		
		return theUserRequestToUpdate;
	}	
	
	@DeleteMapping("/{userRequestId}")
	public UsersRequests deleteUsersRequestsById(@PathVariable("userRequestId") int theId) {
		
		/* will raise an exception only if 'theId' to find isn't in the database */
		/*if (!usersRequestsService.existsById(theId)) {
			throw new UsersRequestsNotFoundException("The User Request ID '" + theId + "' can't be found in DataBase - DeleteById impossible !");
		}
		
		return usersRequestsService.deleteById(theId);*/
		return null;
	}
}
