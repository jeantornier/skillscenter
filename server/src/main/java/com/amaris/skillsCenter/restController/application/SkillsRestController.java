package com.amaris.skillsCenter.restController.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.application.Skills;
import com.amaris.skillsCenter.service.application.SkillsService;

@RestController
@RequestMapping("/skills")
public class SkillsRestController {
	
	@Autowired
	private SkillsService skillsService;
	
	@GetMapping("/all")
	public List<Skills> findAll() {
		return skillsService.findAll();
	}
}
