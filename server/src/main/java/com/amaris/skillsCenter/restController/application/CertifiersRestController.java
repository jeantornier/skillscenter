package com.amaris.skillsCenter.restController.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.application.Certifiers;
import com.amaris.skillsCenter.restController.errorHandlers.Certifications.CertificationsNotFoundException;
import com.amaris.skillsCenter.restController.errorHandlers.Certifiers.CertifiersInputsException;
import com.amaris.skillsCenter.restController.errorHandlers.Certifiers.CertifiersNotFoundException;
import com.amaris.skillsCenter.service.application.CertifiersService;

@RestController
@RequestMapping("/certifiers")
public class CertifiersRestController {
	
	@Autowired
	private CertifiersService certifiersService;
	
	@GetMapping("/all")
	public List<Certifiers> findAll() {
		return certifiersService.findAll();
	}
	
	@GetMapping("/{certifierId}")
	public Certifiers findById(@PathVariable("certifierId") int theId) {
		
		/* will raise an exception only if 'theId' to find isn't in the database */
		if (!certifiersService.existsById(theId)) {
			throw new CertifiersNotFoundException("The Certifier ID '" + theId + "' can't be found in DataBase !");
		}
		
		return certifiersService.findById(theId);
	}
	
	@PostMapping("")
	public Certifiers createCertifiers(@RequestBody Certifiers theCertifier) {
		
		certifiersService.save(theCertifier);
		return theCertifier;
	}
	
	@PutMapping("")
	public Certifiers updateCertifiers(@RequestBody Certifiers theCertifier) {
		
		/* will raise an exception only if 'theId' to find isn't in the database */
		if (!certifiersService.existsById(theCertifier.getId())) {
			throw new CertificationsNotFoundException("The Certifier ID '" + theCertifier.getId() + "' can't be found in DataBase - Update impossible !");
		}
		
		certifiersService.save(theCertifier);
		return theCertifier;
	}
	
	@DeleteMapping("/{certifierId}")
	public Certifiers deleteCertifiersById(@PathVariable("certifierId") int theId) {
		
		Certifiers theCertifierToDelete = null;
		
		/* will raise an exception only if 'theId' to find isn't in the database */
		if (!certifiersService.existsById(theId)) {
			throw new CertifiersNotFoundException("The Certifier ID '" + theId + "' can't be found in DataBase - DeleteById impossible !");
		}
		else {
			
			theCertifierToDelete = certifiersService.findById(theId);
			certifiersService.deleteById(theCertifierToDelete.getId());
		}
		
		return theCertifierToDelete;
	}
	
	
	/* class specific methods */
	@GetMapping("/search/{name}")
	private List<Certifiers> searchCertifiersByName(@PathVariable("name") String nameToSearch) {
		
		List<Certifiers> results = null;
		nameToSearch = nameToSearch.trim();
		
		if (nameToSearch.length() <= 0) {
			throw new CertifiersInputsException("Certifier name to search, '" + nameToSearch + "' is empty !");
		}
		
		results = certifiersService.searchByName(nameToSearch);
		return results;
	}
	
	@GetMapping("/count")
	private long countCertifications() {
				
		return certifiersService.countAllCertifiers();
	}
}
