package com.amaris.skillsCenter.restController.errorHandlers.Certifications;

public class CertificationsNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CertificationsNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CertificationsNotFoundException(String message) {
		super(message);
	}

	public CertificationsNotFoundException(Throwable cause) {
		super(cause);
	}
}
