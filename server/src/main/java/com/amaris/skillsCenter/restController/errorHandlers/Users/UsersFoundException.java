package com.amaris.skillsCenter.restController.errorHandlers.Users;

public class UsersFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersFoundException(String message) {
		super(message);
	}

	public UsersFoundException(Throwable cause) {
		super(cause);
	}
}
