package com.amaris.skillsCenter.restController.errorHandlers.Authorities;

public class AuthoritiesIllegalOperationException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthoritiesIllegalOperationException(String message, Throwable cause) {
		super(message, cause);
	}

	public AuthoritiesIllegalOperationException(String message) {
		super(message);
	}

	public AuthoritiesIllegalOperationException(Throwable cause) {
		super(cause);
	}
}
