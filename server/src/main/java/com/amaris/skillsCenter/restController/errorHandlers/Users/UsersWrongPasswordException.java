package com.amaris.skillsCenter.restController.errorHandlers.Users;

public class UsersWrongPasswordException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersWrongPasswordException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersWrongPasswordException(String message) {
		super(message);
	}

	public UsersWrongPasswordException(Throwable cause) {
		super(cause);
	}
}
