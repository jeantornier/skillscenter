package com.amaris.skillsCenter.restController.errorHandlers.UsersRequests;

public class UsersRequestsFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersRequestsFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersRequestsFoundException(String message) {
		super(message);
	}

	public UsersRequestsFoundException(Throwable cause) {
		super(cause);
	}
}
