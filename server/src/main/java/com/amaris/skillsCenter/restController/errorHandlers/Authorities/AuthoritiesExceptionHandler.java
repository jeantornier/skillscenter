package com.amaris.skillsCenter.restController.errorHandlers.Authorities;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AuthoritiesExceptionHandler {	
	
	@ExceptionHandler
	public ResponseEntity<AuthoritiesErrorResponse> handleException(AuthoritiesIllegalOperationException exc) {
		
		AuthoritiesErrorResponse error = new AuthoritiesErrorResponse();
		
		error.setStatus(HttpStatus.FORBIDDEN.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
	}
	
	@ExceptionHandler
	public ResponseEntity<AuthoritiesErrorResponse> handleException(AuthoritiesNotFoundException exc) {
		
		AuthoritiesErrorResponse error = new AuthoritiesErrorResponse();
		
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<AuthoritiesErrorResponse> handleException(AuthoritiesFoundException exc) {
		
		AuthoritiesErrorResponse error = new AuthoritiesErrorResponse();
		
		error.setStatus(HttpStatus.FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.FOUND);
	}
	
	/* Handle all the exceptions */
	@ExceptionHandler
	public ResponseEntity<AuthoritiesErrorResponse> handleException(Exception exc) {
		
		AuthoritiesErrorResponse error = new AuthoritiesErrorResponse();
		
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}
