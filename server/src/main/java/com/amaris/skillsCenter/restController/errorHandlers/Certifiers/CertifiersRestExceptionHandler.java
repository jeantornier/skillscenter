package com.amaris.skillsCenter.restController.errorHandlers.Certifiers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CertifiersRestExceptionHandler {	
	
	@ExceptionHandler
	public ResponseEntity<CertifiersErrorResponse> handleException(CertifiersNotFoundException exc) {
				
		CertifiersErrorResponse error = new CertifiersErrorResponse();
		
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler
	public ResponseEntity<CertifiersErrorResponse> handleException(CertifiersInputsException exc) {
				
		CertifiersErrorResponse error = new CertifiersErrorResponse();
		
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage("IO Exception on REST API, " + exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
				
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
		
	/* Handle all the exceptions */
	@ExceptionHandler
	public ResponseEntity<CertifiersErrorResponse> handleException(Exception exc) {
		
		CertifiersErrorResponse error = new CertifiersErrorResponse();
		
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}
