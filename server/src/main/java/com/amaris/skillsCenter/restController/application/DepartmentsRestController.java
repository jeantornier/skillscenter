package com.amaris.skillsCenter.restController.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amaris.skillsCenter.model.application.Departments;
import com.amaris.skillsCenter.service.application.DepartmentsService;

@RestController
@RequestMapping("/departments")
public class DepartmentsRestController {
	
	@Autowired
	private DepartmentsService departmentsService;
	
	@GetMapping("/all")
	public List<Departments> findAll() {
		return departmentsService.findAll();
	}
}
