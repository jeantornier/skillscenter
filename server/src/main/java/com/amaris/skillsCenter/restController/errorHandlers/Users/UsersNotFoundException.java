package com.amaris.skillsCenter.restController.errorHandlers.Users;

public class UsersNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsersNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsersNotFoundException(String message) {
		super(message);
	}

	public UsersNotFoundException(Throwable cause) {
		super(cause);
	}
}
