package com.amaris.skillsCenter;

import java.util.Scanner;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import com.amaris.skillsCenter.model.security.UsersTests;

@SpringBootTest
class SkillsCenterApplicationTests {

	@Value("${test.model}")
	private String testModel;
	
	@Autowired
	private UsersTests usersTests;
	
	@Value("${test.service}")
	private String testService;


	@Test
	void contextLoads() {
		
		Scanner input = new Scanner(System.in);
		String userChoice = "no";
		
		System.out.println("\n\t** ** ** Test of the application Skills Center ** ** **\n");
		
		System.out.println("WARNING: in order to run all the Unit Tests, you have to initialize the DataBase with the SQL Script.");
		System.out.println("If this operation isn't done, the Unit Tests result won't be RELEVANT !!!\n");
		
		System.out.println("Do you want to continue [y/yes or n/no] ? ");
		
		userChoice = input.nextLine();
		userChoice = userChoice.toLowerCase();
		input.close();
		
		if (userChoice.equals("n") || userChoice.equals("no")) {
			
			System.out.println("Unit Test INTERRUPTED BY THE USER.");
			System.out.println("\n\t** ** ** Test of the application Skills Center ** ** **\n");
			
		} else { 
		
			testModelLayer();
			testServiceLayer();
			
			System.out.println("Unit Test DONE.");
			System.out.println("\n\t** ** ** Test of the application Skills Center ** ** **\n");
		}
	}
	
	@Test
	public boolean testModelLayer() {
		
		if (!testModel.toLowerCase().equals("true")) { 
			System.out.println("I.   Test of the Model Layer : not to be done");
			return true;
		} 
		
		System.out.println("I.   Test of the Model Layer : in progress ... \n");
		usersTests.runUsersTests();
		
		return true;
	}
	
	@Test
	public boolean testServiceLayer() {
		
		if (!testService.toLowerCase().equals("true")) { 
			System.out.println("II. Test of the Service Layer : not to be done");
			return true;
		} 
		
		System.out.println("II. Test of the Service Layer : in progress ... \n");
		return true;
	}
}
