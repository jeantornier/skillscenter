package com.amaris.skillsCenter.model.security;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UsersTests {

	@Value("${test.service.Users}")
	private String toTest;
	
	public void runUsersTests() {
		
		/* Check if this model test has to be run or not */
		if (toTest.toLowerCase().equals("true")) {
			
			System.out.println("\tTests of the Users\n");
			
			testAddAuthority();
			
		} else {
			
			System.out.println("\tTests of the Users : not required -> IGNORED\n\n");
		}
	}
	
	@Test
	public void testAddAuthority() { 
		
		boolean isAuthorityAdded = false;
		
		Users theUser = new Users();
		Authorities theAuthority = new Authorities("ROLE_TEST_1");
		
		/* start the test */
		System.out.println("\t\t+ Test of method 'addAuthority' ...");

		theAuthority.setId(1);
		theUser.addAuthority(theAuthority);
		
		/* Check if the authority is inside the list of the user */
		for (Authorities theAuthorityFound: theUser.getAuthorities()) {
			
			if (theAuthorityFound == theAuthority) { 
				isAuthorityAdded = true;
			}
		}
		
		assertEquals(true, isAuthorityAdded);
		System.out.println("\t\t| asssert 1/2 : passed");
		assertEquals(1, theUser.getAuthorities().size());
		System.out.println("\t\t| asssert 2/3 : passed");

		/* test if a recall of addAuthority with thesame authority doesn't add it */
		theUser.addAuthority(theAuthority);
		
		assertEquals(1, theUser.getAuthorities().size());
		System.out.println("\t\t| asssert 3/3 : passed");
		System.out.println("\t\t+ - - - - - - - - - - -\n");
	}
}
