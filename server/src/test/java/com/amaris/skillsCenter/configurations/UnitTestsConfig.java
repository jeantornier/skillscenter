package com.amaris.skillsCenter.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:unitTests.properties")
@ComponentScan("com.amaris.skillsCenter")
public class UnitTestsConfig {

	// contains the bean for the unitTests.properties
}
